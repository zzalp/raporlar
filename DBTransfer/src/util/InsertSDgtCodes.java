package util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

public class InsertSDgtCodes {
	private static final String CONFIG_PROPERTIES = "config_bb.properties";
	static Properties prop;
	static MongoDatabase db;
	static MongoCollection<Document> collection;

	public static void main(String[] args) {
		prop = PropertyReader.getProperties(CONFIG_PROPERTIES);
		String MongoIP = prop.getProperty("MONGO_IP");
		MongoClient mongoClient = new MongoClient(MongoIP, 27017);
		db = mongoClient.getDatabase(prop.getProperty("Mongo_DB_Name"));

		collection = db.getCollection("airline3dgtcodes");

		try (BufferedReader br = new BufferedReader(new FileReader("airline3dgtcodes.txt"))) {
			Document airlineDoc = new Document();
			String line;
			while ((line = br.readLine()) != null) {
				String[] airlines = line.split("\t");
				if (airlines.length < 3)
					System.err.println(airlines[0]);
				else {
					airlineDoc.put("_id", airlines[0]);
					airlineDoc.put("airlineCode", airlines[1]);
					airlineDoc.put("3dgtCode", airlines[2]);
					if(airlines[2].length()!=3)
						System.err.println("must be 3 digit: "+airlines[0]);
					collection.insertOne(airlineDoc);
				}
				
				System.out.println(airlineDoc);

			}

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
