package util;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.Properties;

public class PropertyUpdater {
	Properties props;

	public PropertyUpdater(String configFileName) {
		FileInputStream in;
		try {
			in = new FileInputStream(configFileName);
			props = new Properties();
			props.load(in);
			in.close();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void setLastUpdate(Date date, String configFileName) {
		FileOutputStream out;
		try {
			out = new FileOutputStream(configFileName);
			props.setProperty("lastUpdate", date.toString());
			props.store(out, null);
			out.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
