package com.bb.hotel.utils;

import java.util.HashMap;
import java.util.Map;

public class FareReasonEnum {
	public static Map<Integer, String> get() {
		Map<Integer, String> feraReasonType = new HashMap<Integer, String>();
		feraReasonType.put(0, "Buy");
		feraReasonType.put(1, "Sell");
		feraReasonType.put(4, "RefundBuy");
		feraReasonType.put(5, "RefundSell");
		feraReasonType.put(2, "ReissueBuy");
		feraReasonType.put(3, "ReissueSell");
		feraReasonType.put(6, "VoidBuy");
		feraReasonType.put(7, "VoidSell");

		return feraReasonType;

	}
}
