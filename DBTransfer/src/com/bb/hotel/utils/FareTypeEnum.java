package com.bb.hotel.utils;

import java.util.HashMap;
import java.util.Map;

public class FareTypeEnum {
	public static Map<Integer, String> get() {
		Map<Integer, String> tripType = new HashMap<Integer, String>();
		tripType.put(0, "BaseFare"); 
		tripType.put(1, "BaseFareAddOn");
		tripType.put(2, "BaseFareDiscount");
		tripType.put(3, "ProviderServiceCharge"); 
		tripType.put(4, "Tax"); 
		tripType.put(5, "TaxAddon"); 
		tripType.put(6, "TaxDiscount");
		tripType.put(7, "SystemCharge"); 
		tripType.put(8, "LastSellerCommission"); 
		tripType.put(9, "SystemReissueCharge"); 
		tripType.put(10, "LastSellerReissueCharge"); 
		tripType.put(11, "ProviderReissueCharge"); 
		tripType.put(12, "ProviderRefundCharge"); 	
		tripType.put(13, "SystemRefundCharge"); 
		tripType.put(14, "LastSellerRefundCharge"); 
		tripType.put(15, "BFF"); 
		
		return tripType;

	}
}
