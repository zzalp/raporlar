package com.bb.hotel.utils;

import java.util.HashMap;
import java.util.Map;

public class BookingStatusEnum {
	public static Map<Integer, String> get() {
		Map<Integer, String> feraReasonType = new HashMap<Integer, String>();

		feraReasonType.put(10, "PreBooked");
		feraReasonType.put(20, "Reservation");
		feraReasonType.put(30, "Booked");
		feraReasonType.put(40, "Cancelled");
		feraReasonType.put(50, "Expired");
		feraReasonType.put(60, "Provision");
		feraReasonType.put(70, "ReservationPendingCancel");
		feraReasonType.put(80, "Refunded");
		return feraReasonType;

	}
}
