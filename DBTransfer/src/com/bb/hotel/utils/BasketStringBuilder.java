package com.bb.hotel.utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BasketStringBuilder {

	public static StringBuilder get(String[] hundredbaskets) {
		StringBuilder builder = new StringBuilder();
		builder.append("'");
		for (int i = 0; i < hundredbaskets.length; i++) {
			if (hundredbaskets[i] != null) {
				builder.append(hundredbaskets[i]);
				builder.append("','");
			}
		}
		builder.deleteCharAt(builder.length() - 1);
		builder.deleteCharAt(builder.length() - 1);

		return builder;
	}

	public static StringBuilder getPassengerIdsString(StringBuilder bookingIds, String[] hundredbaskets,
			Connection connUlrta) {
		List<String> passengers = new ArrayList<String>();
		PreparedStatement preStatementPassengers;
		try {
			preStatementPassengers = connUlrta.prepareStatement(
					"SELECT distinct Id from flight.Passenger (nolock) where BookingId in (" + bookingIds.toString() + ")",
					ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
			ResultSet resultPassengers = preStatementPassengers.executeQuery();
			resultPassengers.beforeFirst();

			while (resultPassengers.next()) {
				passengers.add(resultPassengers.getString("Id"));
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return get(passengers);
	}

	public static StringBuilder getBookingIdsString(StringBuilder basketIds, String[] hundredbaskets,
			Connection connUlrta) {
		List<String> bookings = new ArrayList<String>();
		PreparedStatement preStatementBookings;
		try {
			preStatementBookings = connUlrta.prepareStatement(
					"SELECT distinct Id from hotel.Booking (nolock) where Id in (" + basketIds.toString() + ")",
					ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
			ResultSet resultBookings = preStatementBookings.executeQuery();
			resultBookings.beforeFirst();

			while (resultBookings.next()) {
				bookings.add(resultBookings.getString("Id"));
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return get(bookings);
	}

	public static StringBuilder get(List<String> list) {
		StringBuilder builder = new StringBuilder();
		builder.append("'");
		for (String id : list) {
			if (id != null) {
				builder.append(id);
				builder.append("','");
			}
		}
		builder.deleteCharAt(builder.length() - 1);
		builder.deleteCharAt(builder.length() - 1);

		return builder;
	}

}
