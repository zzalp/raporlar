package com.bb.flight.db.utils;

import java.util.HashMap;
import java.util.Map;

public class ProductStatusEnum {
	public static Map<Integer, String> get() {
		Map<Integer, String> productStatuses = new HashMap<Integer, String>();
		productStatuses.put(0, "Allocated");
		productStatuses.put(1, "Prebooked");
		productStatuses.put(2, "PrebookingCancelled");
		productStatuses.put(3, "ReservationExpired");
		productStatuses.put(4, "Booked");
		productStatuses.put(5, "BookingCancelled");
		productStatuses.put(6, "BookingReissued");
		productStatuses.put(7, "BookingRefunded");
		productStatuses.put(8, "BookingPartialRefunded");
		productStatuses.put(9, "Unknown");
		productStatuses.put(10, "Reservation");
		productStatuses.put(11, "Void");
		productStatuses.put(12, "Provisioning");
		productStatuses.put(13, "ReservationCancelled");
		return productStatuses;

	}
}
