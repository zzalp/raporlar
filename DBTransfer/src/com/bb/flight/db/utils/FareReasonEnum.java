package com.bb.flight.db.utils;

import java.util.HashMap;
import java.util.Map;

public class FareReasonEnum {
	public static Map<Integer, String> get() {
		Map<Integer, String> feraReasonType = new HashMap<Integer, String>();
		feraReasonType.put(1, "Buy");
		feraReasonType.put(10, "Sell");
		feraReasonType.put(20, "RefundBuy");
		feraReasonType.put(21, "RefundSell");
		feraReasonType.put(30, "Penalty");
		feraReasonType.put(40, "ReissueBuy");
		feraReasonType.put(50, "ReissueSell");
		feraReasonType.put(60, "VoidBuy");
		feraReasonType.put(70, "VoidSell");

		return feraReasonType;

	}
}
