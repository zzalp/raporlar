package com.bb.flight.db.utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class Agencies {
	public static Map<String, HashMap<String, String>> get(Connection connUlrta) {
		Map<String, HashMap<String, String>> businesses = new HashMap<String, HashMap<String, String>>();
		HashMap<String, String> bDetails = new HashMap<String, String>();
		PreparedStatement preStatement;
		try {
			preStatement = connUlrta.prepareStatement("select * from [BB_Accounts].[accounts].[Agency] (NOLOCK)");
			ResultSet result = preStatement.executeQuery();
			while (result.next()) {
				bDetails = new HashMap<String, String>();
				bDetails.put("Name", result.getString("Name").trim());
				bDetails.put("Level", result.getString("AgencyLevel"));
				bDetails.put("ParentAgencyId", result.getString("ParentAgencyId"));
				bDetails.put("City", result.getString("City").trim());
				bDetails.put("Country", result.getString("Country").trim());
				bDetails.put("Status", result.getString("IsActive"));
				bDetails.put("ActivationDate", result.getString("ActivationDate"));
				bDetails.put("DefaultCurrency", result.getString("DefaultCurrency"));
				businesses.put(result.getString("Id").trim(), bDetails);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return businesses;

	}
}
