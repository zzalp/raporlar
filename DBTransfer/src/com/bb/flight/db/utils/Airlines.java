package com.bb.flight.db.utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class Airlines {
	
	public static Map<String, String> get(Connection connUlrta) {
		Map<String, String> airlines = new HashMap<String, String>();
		PreparedStatement preStatementAirportCities;
		try {
			preStatementAirportCities = connUlrta.prepareStatement(
					"SELECT Code,Name FROM [BB_MetaData].[dbo].[Airlines] (nolock)", ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			ResultSet resultAirportCities = preStatementAirportCities.executeQuery();
			resultAirportCities.beforeFirst();

			while (resultAirportCities.next()) {
				airlines.put(resultAirportCities.getString("Code"), resultAirportCities.getString("Name"));
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return airlines;

	}

}
