package com.bb.flight.db.utils;

import java.util.HashMap;
import java.util.Map;

public class PaymentEnum {
	public static Map<Integer, String> get() {
		Map<Integer, String> tripType = new HashMap<Integer, String>();
		tripType.put(0, "PaymentFromRunningAccount");
		tripType.put(1, "CreditCard");
		tripType.put(2, "CreditCard3D");
		tripType.put(3, "MellatBank");

		return tripType;

	}
}
