package com.bb.flight.db.utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class Agents {

	public static Map<Integer, String> get(Connection connUlrta) {
		Map<Integer, String> providers = new HashMap<Integer, String>();
		PreparedStatement preStatementAirportCities;
		try {
			preStatementAirportCities = connUlrta.prepareStatement(
					"SELECT Id, Username FROM [BB_Accounts].[accounts].[User] (nolock)",
					ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
			ResultSet resultAirportCities = preStatementAirportCities.executeQuery();
			resultAirportCities.beforeFirst();

			while (resultAirportCities.next()) {
				providers.put(resultAirportCities.getInt("Id"), resultAirportCities.getString("UserName"));
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return providers;

	}

}
