package com.bb.flight.db.utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Providers {

	public static Map<Integer, List<String>> get(Connection connUlrta) {
		Map<Integer, List<String>> providers = new HashMap<Integer, List<String>>();
		PreparedStatement preStatementAirportCities;
		try {
			preStatementAirportCities = connUlrta.prepareStatement(
					"SELECT Id, ProviderName,BeneficiaryAgencyId FROM [BB_Accounts].[accounts].[Provider] (nolock)",
					ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
			ResultSet resultAirportCities = preStatementAirportCities.executeQuery();
			resultAirportCities.beforeFirst();

			while (resultAirportCities.next()) {
				List<String> providerBeneficiary = new ArrayList();
				providerBeneficiary.add(resultAirportCities.getString("ProviderName"));
				providerBeneficiary.add(resultAirportCities.getString("BeneficiaryAgencyId"));
				providers.put(resultAirportCities.getInt("Id"), providerBeneficiary);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return providers;

	}

}
