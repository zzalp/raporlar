package com.bb.flight.db.utils;

import java.util.HashMap;
import java.util.Map;

public class FareTypeEnum {
	public static Map<Integer, String> get() {
		Map<Integer, String> tripType = new HashMap<Integer, String>();
		tripType.put(1, "BaseFare"); //eklendi
		tripType.put(10, "BaseFareAddOn");
		tripType.put(20, "BaseFareDiscount");
		tripType.put(30, "Taxes"); 
		tripType.put(40, "TaxVq"); //eklendi
		tripType.put(50, "TaxYr"); //eklendi
		tripType.put(60, "TaxQc"); //eklendi
		tripType.put(70, "TaxOthers"); //eklendi
		tripType.put(80, "ProviderServiceCharge"); //eklendi
		tripType.put(90, "SystemCharge"); //eklendi
		tripType.put(100, "LastSellerCommission"); //eklendi
		tripType.put(120, "SystemRefundCharge"); //eklendi
		tripType.put(130, "LastSellerRefundCharge"); //eklendi
		tripType.put(140, "SystemReissueCharge"); //eklendi
		tripType.put(150, "LastSellerReissueCharge"); //eklendi
		tripType.put(160, "BFF");
		tripType.put(170, "ExGap");
		tripType.put(180, "SystemVoidCharge");
		tripType.put(190, "LastSellerVoidCharge");
		return tripType;

	}
}
