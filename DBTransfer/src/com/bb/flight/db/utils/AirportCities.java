package com.bb.flight.db.utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class AirportCities {
	public static Map<String, String> get(Connection connUlrta) {
		Map<String, String> cities = new HashMap<String, String>();
		PreparedStatement preStatementAirportCities;
		try {
			preStatementAirportCities = connUlrta.prepareStatement(
					"SELECT Code, Name FROM [BB_MetaData].[dbo].[Cities] (nolock)", ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			ResultSet resultAirportCities = preStatementAirportCities.executeQuery();
			resultAirportCities.beforeFirst();

			while (resultAirportCities.next()) {
				cities.put(resultAirportCities.getString("Code"), resultAirportCities.getString("Name"));
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return cities;

	}
}
