package com.bb.flight.db.utils;

import java.util.HashMap;
import java.util.Map;

public class GenderType {
	public static Map<Integer, String> get() {
		Map<Integer, String> genderType = new HashMap<Integer, String>();
		genderType.put(0, "None");
		genderType.put(1, "Male");
		genderType.put(2, "Female");
		return genderType;

	}
}
