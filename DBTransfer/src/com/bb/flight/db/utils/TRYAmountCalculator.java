package com.bb.flight.db.utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class TRYAmountCalculator {

	public static Double get(Connection connUlrta, int freeDataId, String toCurr) {
		Double rate = null;
		PreparedStatement preStatementFreeData;
		try {
			preStatementFreeData = connUlrta.prepareStatement(
					"select * from flight.FreeData (NOLOCK) where Id=" + freeDataId, ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			ResultSet resultFreeData = preStatementFreeData.executeQuery();
			resultFreeData.beforeFirst();

			while (resultFreeData.next()) {
				JSONObject freedata = new JSONObject(resultFreeData.getString("StringData"));
				
				// System.out.println(freedata.getString("SaleCurrency"));
				JSONArray arr = freedata.getJSONArray("SaleExchangeRates");
				for (int i = 0; i < arr.length(); i++) {
					String fromCurrency = arr.getJSONObject(i).getString("FromCurrency");
					String toCurrency = arr.getJSONObject(i).getString("ToCurrency");
					if (fromCurrency.equals("TRY") && toCurrency.equals(toCurr)) {
						rate = arr.getJSONObject(i).getDouble("Rate");
					}
				}
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (JSONException e) {
			// TODO Auto-generated catch block
			System.out.println(freeDataId);
		}
		
		
		return rate;
	}

}
