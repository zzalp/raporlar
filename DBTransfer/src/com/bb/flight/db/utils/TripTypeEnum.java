package com.bb.flight.db.utils;

import java.util.HashMap;
import java.util.Map;

public class TripTypeEnum {
	public static Map<Integer, String> get() {
		Map<Integer, String> tripType = new HashMap<Integer, String>();
		tripType.put(0, "OneWay");
		tripType.put(1, "RoundTrip");
		tripType.put(2, "Multiple Point");

		return tripType;

	}
}
