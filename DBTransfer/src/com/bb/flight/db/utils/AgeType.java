package com.bb.flight.db.utils;

import java.util.HashMap;
import java.util.Map;

public class AgeType {
	public static Map<Integer, String> get() {
		Map<Integer, String> ageType = new HashMap<Integer, String>();
		ageType.put(0, "Adt");
		ageType.put(1, "Chd");
		ageType.put(2, "Inf");
		ageType.put(3, "Std");
		ageType.put(4, "Yth");
		ageType.put(5, "Src");
		ageType.put(6, "Mil");
		ageType.put(7, "Sea");
		ageType.put(8, "Lbr");

		return ageType;

	}
}
