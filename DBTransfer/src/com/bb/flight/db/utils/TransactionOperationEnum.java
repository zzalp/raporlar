package com.bb.flight.db.utils;

import java.util.HashMap;
import java.util.Map;

public class TransactionOperationEnum {
	public static Map<Integer, String> get() {
		Map<Integer, String> operation = new HashMap<Integer, String>();

		operation.put(10, "Booking");
		operation.put(20, "Reissue");
		operation.put(30, "Refund");
		operation.put(40, "Void");
		operation.put(50, "Update");

		return operation;

	}
}
