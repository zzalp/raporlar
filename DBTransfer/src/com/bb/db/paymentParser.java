package com.bb.db;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.bson.Document;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.mongodb.MongoClient;
import com.mongodb.MongoWriteException;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

import util.PropertyReader;

public class paymentParser {

	private static final String CONFIG_PROPERTIES = "config_bb.properties";
	static DateFormat format;
	static Connection connTrevoo;
	static Connection connBiletBank;
	static Properties prop;
	static MongoDatabase db;
	static int insertCount = 0;
	static int updateCount = 0;
	static MongoCollection<Document> collection;
	static ResultSet resultPayments;
	static Map<String, HashMap<String, String>> businesses;
	static Map<Integer, String> customerUsers;
	static Map<String, String> bins;
	static Map<String, String> cardType;
	static long startTime;
	static int conj;

	public static void main(String[] args) {
		conj = 0;
		format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		prop = new Properties();
		prop = PropertyReader.getProperties(CONFIG_PROPERTIES);
		connTrevoo = connect(prop.getProperty("maindb"));

		String MongoIP = prop.getProperty("MONGO_IP");
		MongoClient mongoClient = new MongoClient(MongoIP, 27017);
		db = mongoClient.getDatabase(prop.getProperty("Mongo_DB_Name"));

		collection = db.getCollection("allPayments");
		customerUsers = readCustomerUsers();

		bins = readBins();
		cardType = readCardType();
		// System.out.println(cardType);
		businesses = readBusinesses();
		getNewRecords(prop.get("lastUpdate").toString(), prop.get("updateUntil").toString());

		System.out.println("inserted:" + insertCount);
		System.out.println("updated:" + updateCount);
		mongoClient.close();
		// updatePropertiesFile();

	}

	private static Map<String, HashMap<String, String>> readBusinesses() {
		Map<String, HashMap<String, String>> businesses = new HashMap<String, HashMap<String, String>>();
		HashMap<String, String> bDetails = new HashMap<String, String>();
		PreparedStatement preStatement;
		try {
			preStatement = connTrevoo.prepareStatement(
					"select Id, Level, Name, SuperBusinessId, Location_Detail, Location_District,Location_Street,Location_Neighborhoods, Location_City, Location_Country, AgencyType, Status_IfActive, Status_IfForbidden, Activation_TransactionDate,GroupId from Accounts.Businesses (NOLOCK) where Level<4");
			ResultSet result = preStatement.executeQuery();
			while (result.next()) {
				bDetails = new HashMap<String, String>();
				bDetails.put("Name", result.getString("Name").trim());
				bDetails.put("Level", result.getString("Level"));
				bDetails.put("SuperBusinessId", result.getString("SuperBusinessId"));
				bDetails.put("Location_City", result.getString("Location_City").trim());
				bDetails.put("Location_Country", result.getString("Location_Country").trim());
				bDetails.put("AgencyType", result.getString("AgencyType"));
				bDetails.put("Status_IfActive", result.getString("Status_IfActive"));
				bDetails.put("Status_IfForbidden", result.getString("Status_IfForbidden"));
				bDetails.put("Activation_TransactionDate", result.getString("Activation_TransactionDate"));
				businesses.put(result.getString("Id").trim(), bDetails);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return businesses;
	}

	private static void updatePropertiesFile() {

		DateTimeFormatter fmt = DateTimeFormat.forPattern("YYYY-MM-dd HH:mm:ss");
		DateTime dt = fmt.parseDateTime(prop.getProperty("updatePaymentsUntil"));

		dt = dt.plusDays(1);
		OutputStream output = null;
		try {
			output = new FileOutputStream(CONFIG_PROPERTIES);

			prop.setProperty("updatePaymentsFrom", prop.getProperty("updatePaymentsUntil"));
			prop.setProperty("updatePaymentsUntil", fmt.print(dt));
			prop.store(output, null);

			output.close();
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static Map<String, String> readBins() {
		Map<String, String> bins = new HashMap<String, String>();
		PreparedStatement preStatement;
		try {
			preStatement = connTrevoo.prepareStatement(
					"SELECT BinNumber, BankName FROM Trevoo.dbo.CC_BinList_TR (nolock) where BankName is not null");
			ResultSet result = preStatement.executeQuery();
			while (result.next()) {
				// System.out.println(result.getString("BinNumber").trim());
				// if (result.getString("BinNumber").trim().equals("459026"))
				// System.out.println(result.getString("BinNumber").trim() + " -
				// " + result.getString("BankName"));
				bins.put(result.getString("BinNumber").trim(), result.getString("BankName").trim());
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return bins;
	}

	private static Map<String, String> readCardType() {
		Map<String, String> carddype = new HashMap<String, String>();
		String type;
		PreparedStatement preStatement;
		try {
			preStatement = connTrevoo.prepareStatement(
					"SELECT BinNumber, SubType FROM Trevoo.dbo.CC_BinList_TR (nolock) where BankName is not null");
			ResultSet result = preStatement.executeQuery();
			while (result.next()) {
				// System.out.println(result.getString("BinNumber").trim());
				// if (result.getString("BinNumber").trim().equals("459026"))
				// System.out.println(result.getString("BinNumber").trim() + " -
				// " + result.getString("BankName"));
				try {
					if (result.getString("SubType").trim().toLowerCase().equals("debit"))
						type = "Debit Card";
					else
						type = "Credit Card";
				} catch (NullPointerException e) {
					type = "Credit Card";
				}

				carddype.put(result.getString("BinNumber").trim(), type);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return carddype;
	}

	private static Map<Integer, String> readCustomerUsers() {
		Map<Integer, String> users = new HashMap<>();
		PreparedStatement preStatement;
		try {
			preStatement = connTrevoo.prepareStatement("select Id, UserName from Accounts.Users (NOLOCK)");
			ResultSet result = preStatement.executeQuery();
			while (result.next()) {
				users.put(result.getInt("Id"), result.getString("Username").trim());
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return users;
	}

	private static void getNewRecords(String fromDate, String toDate) {
		Document payment;
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		PreparedStatement preStatement, preStatementBasket;
		int count = 0;
		try {
			
			preStatement = connTrevoo.prepareStatement("select	prod.ProductType,m.CC_IsPosDevice,m.Id,a.BusinessId,isnull(vpos.Bank, b.[Name]) AS CC_VPos_BankName,m.FileId as ShoppingFileId, m.Method AS PaymentType,m.ActionDateLocal as date,"
					+ "(case m.IsReceive when 1 then m.CC_CardNo else parent.CC_CardNo end) as CC_CardNumber,(case m.IsReceive when 1 then m.XCredit else 0 end) as Payment_ValueConfirmed,	(case m.IsSend when 1 then m.XDebt else 0 end)  as Payment_ValueCredit,"
					+ "(case m.IsReceive when 1 then m.CC_InstallmentCount else parent.CC_InstallmentCount end) as CC_InstallmentCount	from Accounting2014.MoneyFlow m with(nolock) inner join Accounting2014.AgencyAccount a (nolock) on a.Id = m.XAccountId "
					+ "left join Accounting.Payment_VPos_Definitions vpos (nolock) on vpos.Id = m.ViaAccountId "
					+ "left join Accounting2014.PosAccount pos (nolock) on pos.Id = m.ViaAccountId "
					+ "left join Accounting2014.BankAccount bn (nolock) on bn.Id = m.ViaAccountId "
					+ "left join Accounting2014.Bank b (nolock) on b.Id = pos.BankId or b.Id = bn.BankId	"
					+ "outer apply (select top 1 ProductType from Sale.ShoppingFile_Products p (nolock) where p.ShoppingFileId = m.FileId order by p.[Index] desc) prod	outer apply ("
					+ "select top 1 mp.CC_CardNo, mp.CC_InstallmentCount from Accounting2014.MoneyFlow mp with(nolock) "
					+ "where mp.Id = m.ParentFlowId) parent	where m.Method not in ('INV','PERIOD_LIMIT') and IsPostDeposit = 0 and IsVirman = 0 and IsTurnover = 0 "
					+ "and	m.ActionDateLocal >= '" + fromDate + "' and m.ActionDateLocal < '" + toDate + "'"
					+ "union all "
					+ "SELECT	prod.ProductType, null as CC_IsPosDevice,	p.Id, p.BusinessId, null AS CC_VPos_BankName, p.ShoppingFileId, SUBSTRING(p.CC_CardHolder, 0, CHARINDEX('/', p.CC_CardHolder)) as PaymentType, dateadd(HOUR, 3, p.T_PostAuth_TransactionDate) as date,"
					+ "p.CC_CardNumber,	p.Payment_ValueConfirmed,p.Payment_ValueCredit,	p.CC_InstallmentCount FROM Sale.ShoppingFile_Payments p with(NOLOCK) left join Accounting2014.BookingItemLine bil with(nolock) on bil.Id = p.Id outer apply ("
					+ "select top 1 ProductType from Sale.ShoppingFile_Products pd (nolock) where pd.ShoppingFileId = p.ShoppingFileId order by pd.[Index] desc	) prod "
					+ "where p.PaymentType = 'MP' and (p.CC_CardHolder like 'MUS. KARTI/%' or p.CC_CardHolder like 'UATP/%') "
					+ "and dateadd(HOUR, 3, T_PostAuth_TransactionDate)>='"+fromDate+"' and dateadd(HOUR, 3, T_PostAuth_TransactionDate)<'" + toDate + "'");

//			preStatement = connTrevoo.prepareStatement(
//					"select prod.ProductType,m.CC_IsPosDevice,m.Id,a.BusinessId,isnull(vpos.Bank, b.[Name]) AS CC_VPos_BankName, m.FileId as ShoppingFileId, m.Method AS PaymentType,"
//					+ "m.ActionDateLocal as date,m.CC_CardNo as CC_CardNumber,(case m.IsReceive when 1 then m.XCredit else 0 end) as Payment_ValueConfirmed,"
//					+ "(case m.IsSend when 1 then m.XDebt else 0 end)  as Payment_ValueCredit,m.CC_InstallmentCount "
//					+ "from Accounting2014.MoneyFlow m with(nolock) "
//					+ "inner join Accounting2014.AgencyAccount a (nolock) on a.Id = m.XAccountId "
//					+ "left join Accounting.Payment_VPos_Definitions vpos (nolock) on vpos.Id = m.ViaAccountId "
//					+ "left join Accounting2014.PosAccount pos (nolock) on pos.Id = m.ViaAccountId "
//					+ "left join Accounting2014.BankAccount bn (nolock) on bn.Id = m.ViaAccountId "
//					+ "left join Accounting2014.Bank b (nolock) on b.Id = pos.BankId or b.Id = bn.BankId "
//					+ "outer apply (select top 1 ProductType from Sale.ShoppingFile_Products p (nolock) "
//					+ "where p.ShoppingFileId = m.FileId order by p.[Index] desc) prod where m.Method not in ('INV','PERIOD_LIMIT') "
//					+ "and IsPostDeposit = 0 and IsVirman = 0 and IsTurnover = 0 and "
//					+ "dateadd(HOUR, 3, m.ActionDateLocal)>='"+ fromDate +"' and dateadd(HOUR, 3, m.ActionDateLocal)<'" + toDate + "'" 
//					+ "union all "
//					+ "SELECT prod.ProductType, null as CC_IsPosDevice,p.Id, p.BusinessId, null AS CC_VPos_BankName, p.ShoppingFileId, "
//					+ "SUBSTRING(p.CC_CardHolder, 0, CHARINDEX('/', p.CC_CardHolder)) as PaymentType,"
//					+ "dateadd(HOUR, 3, p.T_PostAuth_TransactionDate) as date,"
//					+ "p.CC_CardNumber,p.Payment_ValueConfirmed,p.Payment_ValueCredit,p.CC_InstallmentCount FROM Sale.ShoppingFile_Payments p with(NOLOCK) "
//					+ "left join Accounting2014.BookingItemLine bil with(nolock) on bil.Id = p.Id "
//					+ "outer apply (select top 1 ProductType from Sale.ShoppingFile_Products pd (nolock) "
//					+ "where pd.ShoppingFileId = p.ShoppingFileId order by pd.[Index] desc) prod "
//					+ "where p.PaymentType = 'MP' and (p.CC_CardHolder like 'MUS. KARTI/%' or p.CC_CardHolder like 'UATP/%') "
//					+ "and dateadd(HOUR, 3, T_PostAuth_TransactionDate)>='"
//					+ fromDate + "' and dateadd(HOUR, 3, T_PostAuth_TransactionDate)<'" + toDate + "'");
			System.err.println(preStatement);
			ResultSet result = preStatement.executeQuery();
			while (result.next()) {
				if (count % 1000 == 0)
					System.out.println(count);
				count++;

				payment = new Document();
				Timestamp date = result.getTimestamp("date");
				payment.put("_id", result.getString("Id"));
				if (date != null) {
					payment.put("Date", format.parse(date.toString()));
				}
				payment.put("AgencyId", result.getInt("BusinessId"));
				int productType =result.getInt("ProductType");
				if(productType==1)
					payment.put("ProductType", "Flight");
				else if (productType==2)
					payment.put("ProductType", "Hotel");
				else if (productType==3)
					payment.put("ProductType", "Car");
				else if (productType==4)
					payment.put("ProductType", "Transfer");
				else if (productType==5)
					payment.put("ProductType", "Payment");
				else if (productType==61)
					payment.put("ProductType", "Insurance");
				else if (productType==99)
					payment.put("ProductType", "Test");
				else
					payment.put("ProductType", "None");
				
				
				payment.put("Agency", businesses.get(result.getString("BusinessId")).get("Name"));
				payment.put("AgencyCity", businesses.get(result.getString("BusinessId")).get("Location_City"));
				preStatementBasket = connTrevoo.prepareStatement(
						"select U.Id,U.Username from Trevoo.Sale.ShoppingFiles (nolock) SF join Accounts.Users U (nolock) on SF.Customer_UserId=U.Id where SF.Id = '"
								+ result.getString("ShoppingFileId") + "'");

				ResultSet resultBasket = preStatementBasket.executeQuery();
				while (resultBasket.next()) {
					payment.put("AgencyUser", resultBasket.getString("Username"));
					// System.out.println("username:"+resultBasket.getString("Username"));
				}

				payment = getIsDirectDeposit(payment, result.getString("Id"));
				String paymentType = result.getString("PaymentType");
				if (!"CC".equals(paymentType)) {
					paymentType = paymentType.split("/")[0];
				}
				payment.put("paymentType", paymentType);
				if ("CC".equals(paymentType)) {
					if (result.getString("CC_CardNumber") != null) {
						payment.put("CCNumber", result.getString("CC_CardNumber").replace("-", "").subSequence(0, 6));
						payment.put("CCCardNumber", result.getString("CC_CardNumber"));
					} else {
						preStatement = connTrevoo.prepareStatement(
								"select CC_CardNo, XDebt, IsDirectDeposit FROM [Trevoo].[Accounting2014].[MoneyFlow] (nolock) where FileId='"
										+ result.getString("ShoppingFileId")
										+ "' and Method='CC' and CC_CardNo is not null");
						// System.err.println(preStatement);
						ResultSet resultFlow = preStatement.executeQuery();
						while (resultFlow.next()) {
							// System.out.println(result.getString("Id")+"\t"+result.getString("ShoppingFileId")+"\t"+resultFlow.getString("CC_CardNo"));
							payment.put("CCNumber",
									resultFlow.getString("CC_CardNo").replace("-", "").subSequence(0, 6));
						}
					}
					payment.put("InstallmentCount",
							result.getObject("CC_InstallmentCount") == null ? 0 : result.getInt("CC_InstallmentCount"));
					payment.put("VPos", result.getString("CC_VPos_BankName"));
				}
				if (payment.getString("CCNumber") != null) {
					payment.put("Bank", bins.get(payment.getString("CCNumber")));
					payment.put("CardType", cardType.get(payment.getString("CCNumber")));
					if (result.getBoolean("CC_IsPosDevice") == true)
						payment.put("CardType", "PhysicalPOS");
				}
				if (result.getDouble("Payment_ValueCredit") > 0) {
					payment.put("debitOrCredit", "Refund");
					payment.put("Amount", result.getDouble("Payment_ValueCredit") * -1);
				} else {
					payment.put("debitOrCredit", "Sales");
					payment.put("Amount", result.getDouble("Payment_ValueConfirmed"));
				}

				try {
					collection.insertOne(payment);
					insertCount++;

				} catch (MongoWriteException dk) {
					updateCount++;
					collection.replaceOne(new Document("_id", payment.getString("_id")), payment);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private static Document getIsDirectDeposit(Document payment, String paymentId) {
		try {
			PreparedStatement preStatement = connTrevoo.prepareStatement(
					"SELECT IsDirectDeposit, XDebt from Trevoo.Accounting2014.MoneyFlow where Id='" + paymentId + "'");
			ResultSet result = preStatement.executeQuery();
			while (result.next()) {
				payment.put("IsDirectDeposit", result.getInt("IsDirectDeposit"));
				payment.put("PosComm", result.getDouble("XDebt"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return payment;
	}

	private static Connection connect(String dbName) {
		Connection conn = null;
		try {
			Class.forName("net.sourceforge.jtds.jdbc.Driver");
			conn = DriverManager.getConnection(
					"jdbc:jtds:sqlserver://" + prop.getProperty("maindatabaseIP") + "/" + dbName,
					prop.getProperty("maindbuser"), prop.getProperty("maindbpassword"));
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return conn;
	}

}
