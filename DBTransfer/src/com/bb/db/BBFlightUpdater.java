package com.bb.db;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;

import org.bson.Document;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONException;
import org.json.JSONObject;

import com.mongodb.BasicDBObject;
import com.mongodb.Block;
import com.mongodb.MongoClient;
import com.mongodb.MongoWriteException;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;

import util.PropertyReader;

public class BBFlightUpdater {

	private static final String CONFIG_PROPERTIES = "config_bb.properties";
	private static final int TIME_DIFFERENCE = 3;
	static DateFormat format;
	static Connection connTrevoo;
	static Connection connPetproApp_Data2013;
	static Connection connBiletBank;
	static Properties prop;
	static MongoDatabase db;
	static int insertCount = 0;
	static int updateCount = 0;
	static MongoCollection<Document> collection;
	static ResultSet resultBaskets;
	static ResultSet resultProducts;
	static ResultSet resultTickets;
	static ResultSet resultSegments;
	static ResultSet resultPassengers;
	static ResultSet resultCommissions;
	static ResultSet resultBusinesses;
	static ResultSet resultPayments;
	static ResultSet resultAgencyAccountTransaction;
	static ResultSet resultAmadeusTaxYQ;
	static ResultSet resultAmadeusTaxYR;

	static Document basket;
	static Document product;
	static Document ticket;
	static Document segment;
	static Document passenger;

	static Map<String, String> airportCities;
	static Map<String, String> airportContinents;
	static Map<String, String> airportCountries;
	static Map<String, String> airportCountryNames;
	static Map<String, String> airlines;
	static Map<String, String> airline3dgtCodes;
	static Map<String, HashMap<String, String>> businesses;
	static Map<String, HashMap<String, String>> airlineCommissions;
	static Map<Integer, String> customerUsers;
	static Map<String, String> providers;
	static Map<String, String> branches;
	static Map<Integer, String> businessGroups;
	static Set<Integer> troyaAgencies;
	static Map<Integer, String> productStatuses;
	static long startTime;
	static int conj;

	public static void main(String[] args) {
		conj = 0;
		format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		prop = new Properties();
		prop = PropertyReader.getProperties(CONFIG_PROPERTIES);
		connTrevoo = connect(prop.getProperty("maindb"), prop.getProperty("maindatabaseIP"),
				prop.getProperty("maindbuser"), prop.getProperty("maindbpassword"));
		connBiletBank = connect("BiletBank", prop.getProperty("maindatabaseIP"), prop.getProperty("maindbuser"),
				prop.getProperty("maindbpassword"));
		connPetproApp_Data2013 = connect("PetProApp_data_2013", prop.getProperty("maindatabaseIP"),
				prop.getProperty("maindbuser"), prop.getProperty("maindbpassword"));

		String MongoIP = prop.getProperty("MONGO_IP");
		MongoClient mongoClient = new MongoClient(MongoIP, 27017);
		db = mongoClient.getDatabase(prop.getProperty("Mongo_DB_Name"));

		collection = db.getCollection(prop.getProperty("MONGO_COLLECTION_NAME"));

		Set<String> updatedBaskets = new HashSet<>();

		troyaAgencies = new HashSet<Integer>();
		troyaAgencies = getTroyaAgencies();

		airportCities = new HashMap<>();
		airportCountries = new HashMap<>();
		airportCountryNames = new HashMap<>();
		airportContinents = new HashMap<>();
		airlines = new HashMap<>();
		businesses = new HashMap<>();
		airlineCommissions = new HashMap<>();
		customerUsers = new HashMap<>();

		airportCities = readAirportCities();

		airportCities.put("ANK", "Ankara");
		airportCities.put("IZM", "Izmir");
		airportCities.put("BTZ", "Bursa");
		airportCountries = readAirportCountries();
		airportCountryNames = readAirportCountryNames();
		airportContinents = readAirportContinents();
		airlines = readAirlines();

		airline3dgtCodes = readAirline3dgtCodes();
		// System.out.println(airline3dgtCodes);
		branches = readBraches();
		businessGroups = readBusinessGroups();

		providers = readProviders();
		productStatuses = getProductStatusses();
		businesses = readBusinesses();
		airlineCommissions = readComm();
		customerUsers = readCustomerUsers();

		// if (args[0].equals("day")) {
		// updatedBaskets =
		// readUpdatedBasketIds(prop.get("lastUpdate").toString(),
		// prop.get("updateUntil").toString());
		// }
		// if (args[0].equals("onehour")) {
		// updatedBaskets =
		// readUpdatedBasketIds(prop.get("lastUpdatehourly").toString(),
		// prop.get("updateUntilhourly").toString());
		//// }

		updatedBaskets = readUpdatedBasketIds(prop.get("lastUpdate").toString(), prop.get("updateUntil").toString());
		//
//		 updatedBaskets = readWrongDatedBasketIds();
//		updatedBaskets = readNonExistingBasketIds(prop.get("batchLastUpdate").toString(),
//				prop.get("batchUpdateUntil").toString());

		// updatedBaskets.add(args[0]);
		// updatedBaskets.add("F734401F-798C-48F9-AE2C-8FA526C76282");
		// updatedBaskets = readbyQuery();
		// updatedBaskets = readbySQLQuery();

		System.out.println(updatedBaskets.size() + " baskets will be updated/added");

		int batchCount = Integer.parseInt(prop.getProperty("BATCH_COUNT"));
		System.out.println(batchCount);
		int order = 0;
		int binCount = 0;
		String[] hundredbaskets = new String[batchCount];
		for (String basketId : updatedBaskets) {
			hundredbaskets[order] = basketId;
			order++;
			if (order == batchCount) {
				order = 0;
				binCount++;
				// if (binCount <= Integer.parseInt(args[0]))
				// continue;
				// else {
				update(hundredbaskets);
				hundredbaskets = new String[batchCount];
				// }
				if ((binCount * batchCount) % 1000 == 0)
					System.out.println((binCount * batchCount) + " baskets processed "
							+ new Timestamp(System.currentTimeMillis()));

			}
		}
		if (hundredbaskets[0] != null)
			update(hundredbaskets);

		System.out.println("# baskets inserted:\t" + insertCount);
		System.out.println("# baskets updated:\t" + updateCount);

		// if (args[0].equals("day")) {
		 updatePropertiesFile();
		// }
		// if (args[0].equals("onehour")) {
		// updatePropertiesFilebyThreeHour();
		// }
		// updatePropertiesFileByMonth();
		// updatePropertiesFileByHour(Integer.parseInt(args[0]));

		mongoClient.close();
		close(connTrevoo);
	}

	private static Set<Integer> getTroyaAgencies() {
		Set<Integer> agencies = new HashSet<Integer>();
		String agencyIds = prop.getProperty("TroyaDownloadAgencies");
		String[] array = agencyIds.split(",");

		for (int k = 0; k < array.length; k++) {
			agencies.add(Integer.parseInt(array[k]));
		}

		return agencies;
	}

	private static Map<Integer, String> readBusinessGroups() {

		Map<Integer, String> groups = new HashMap<>();
		PreparedStatement preStatement;
		try {

			preStatement = connTrevoo.prepareStatement("select Id, Code from Accounts.BusinessGroups (NOLOCK)");
			ResultSet result = preStatement.executeQuery();
			while (result.next()) {
				groups.put(result.getInt("Id"), result.getString("Code"));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return groups;
	}

	private static Set<String> readbySQLQuery() {
		Set<String> updatedBaskets = new HashSet<>();
		PreparedStatement preStatement;
		try {
			preStatement = connTrevoo.prepareStatement(
					"SELECT distinct ShoppingFileId FROM [Trevoo].[Sale].ShoppingFile_FlightSegments (NOLOCK) where MarketingAirline='G3' or OperatingAirline='G3' ");
			System.err.println(preStatement);
			ResultSet result = preStatement.executeQuery();
			while (result.next()) {
				updatedBaskets.add(result.getString("ShoppingFileId"));
			}
			System.out.println(updatedBaskets.size());

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return updatedBaskets;
	}

	private static void updatePropertiesFileByHour(int i) {
		DateTimeFormatter fmt = DateTimeFormat.forPattern("YYYY-MM-dd HH:mm:ss");
		DateTime dt = fmt.parseDateTime(prop.getProperty("updateUntilhourly"));

		dt = dt.plusMinutes(i * 30);
		OutputStream output = null;
		try {
			output = new FileOutputStream(CONFIG_PROPERTIES);

			prop.setProperty("lastUpdatehourly", prop.getProperty("updateUntilhourly"));
			prop.setProperty("updateUntilhourly", fmt.print(dt));
			prop.store(output, null);

			output.close();
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private static Set<String> readbyQuery() {
		System.out.println("reading wrong dated basket ids");
		Set<String> invalidDatedBaskets = new HashSet<>();
		MongoCollection<Document> collection = db.getCollection("baskets");

		BasicDBObject inQuery = new BasicDBObject();

		inQuery.put("Products.Tickets.Payback", new Document("$gt", 0));

		MongoCursor<Document> cursor = collection.find(inQuery).iterator();

		try {
			while (cursor.hasNext()) {
				invalidDatedBaskets.add(cursor.next().get("_id").toString());
			}
		} finally {
			cursor.close();
		}

		return invalidDatedBaskets;
	}

	private static Map<Integer, String> readCustomerUsers() {
		Map<Integer, String> users = new HashMap<>();
		PreparedStatement preStatement;
		try {
			preStatement = connTrevoo.prepareStatement("select Id, UserName from Accounts.Users (NOLOCK)");
			ResultSet result = preStatement.executeQuery();
			while (result.next()) {
				users.put(result.getInt("Id"), result.getString("Username").trim());
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return users;
	}

	private static Map<String, String> readAirline3dgtCodes() {
		final Map<String, String> airlinecodes = new HashMap<String, String>();
		FindIterable<Document> iterable = db.getCollection("airline3dgtcodes").find();

		iterable.forEach(new Block<Document>() {
			@Override
			public void apply(final Document document) {
				airlinecodes.put(document.getString("3dgtCode"), document.getString("_id"));
			}
		});

		return airlinecodes;
	}

	private static void close(Connection conn2) {
		try {
			connTrevoo.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	private static void updatePropertiesFile() {

		DateTimeFormatter fmt = DateTimeFormat.forPattern("YYYY-MM-dd HH:mm:ss");
		DateTime lu = fmt.parseDateTime(prop.getProperty("lastUpdate"));
		DateTime uu = fmt.parseDateTime(prop.getProperty("updateUntil"));

		uu = uu.plusDays(1);
		lu = lu.plusDays(1);
		OutputStream output = null;
		try {
			output = new FileOutputStream(CONFIG_PROPERTIES);

			prop.setProperty("lastUpdate", fmt.print(lu));
			prop.setProperty("updateUntil", fmt.print(uu));
			prop.store(output, null);

			output.close();
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static void updatePropertiesFilebyThreeHour() {

		DateTimeFormatter fmt = DateTimeFormat.forPattern("YYYY-MM-dd HH:mm:ss");
		DateTime lu = fmt.parseDateTime(prop.getProperty("lastUpdatehourly"));
		DateTime uu = fmt.parseDateTime(prop.getProperty("updateUntilhourly"));

		uu = uu.plusHours(1);
		lu = lu.plusHours(1);
		OutputStream output = null;
		try {
			output = new FileOutputStream(CONFIG_PROPERTIES);

			prop.setProperty("lastUpdatehourly", fmt.print(lu));
			prop.setProperty("updateUntilhourly", fmt.print(uu));
			prop.store(output, null);

			output.close();
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static void updatePropertiesFileByMonth() {

		DateTimeFormatter fmt = DateTimeFormat.forPattern("YYYY-MM-dd HH:mm:ss");
		DateTime dt = fmt.parseDateTime(prop.getProperty("updateUntil"));

		dt = dt.plusMonths(1);
		OutputStream output = null;
		try {
			output = new FileOutputStream(CONFIG_PROPERTIES);

			prop.setProperty("lastUpdate", prop.getProperty("updateUntil"));
			prop.setProperty("updateUntil", fmt.print(dt));
			prop.store(output, null);

			output.close();
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static Set<String> readUpdatedBasketIds(String lastUpdateDate, String updateUntil) {
		Set<String> updatedBaskets = new HashSet<>();
		Set<String> updatedflightBaskets = new HashSet<>();
		PreparedStatement preStatement;
		try {
			preStatement = connTrevoo.prepareStatement(
					"select distinct Id from Sale.ShoppingFiles (NOLOCK) where dateadd(HOUR, 3, T_LastTransactionDate) >='"
							+ lastUpdateDate + "' and dateadd(HOUR, 3, T_LastTransactionDate) <'" + updateUntil
							+ "'  union select distinct Id from Sale.ShoppingFiles (NOLOCK) where dateadd(HOUR, 3, CreationDate) >='"
							+ lastUpdateDate + "'and dateadd(HOUR, 3, CreationDate) <'" + updateUntil + "'");
			System.err.println(preStatement);
			ResultSet result = preStatement.executeQuery();
			while (result.next()) {
				updatedBaskets.add(result.getString("Id"));
			}
			System.out.println(updatedBaskets.size());
			StringBuilder basketIds = basketIdBuilder(updatedBaskets.toArray());

			PreparedStatement preStatementProducts = connTrevoo.prepareStatement(
					"select ShoppingFileId from Sale.ShoppingFile_Products (NOLOCK) where ShoppingFileId in ("
							+ basketIds.toString()
							+ ") and ProductType = 1 and Status in (12,18,19,20,21) and BookingCode!='*MAHSUP'",
					ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
			resultProducts = preStatementProducts.executeQuery();

			while (resultProducts.next()) {
				updatedflightBaskets.add(resultProducts.getString("ShoppingFileId"));
			}
			System.out.println(updatedflightBaskets.size());
			PreparedStatement preStatementRemoved = connTrevoo.prepareStatement(
					"SELECT S.Id from  Sale.ShoppingFiles (nolock) S left join [Trevoo].[Sale].ShoppingFile_Products P (NOLOCK) on P.ShoppingFileId=S.Id where S.Id in ("
							+ basketIds.toString() + ") and ShoppingFileId is NULL",
					ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
			ResultSet resultRemoved = preStatementRemoved.executeQuery();

			while (resultRemoved.next()) {
				updatedflightBaskets.add(resultRemoved.getString("Id"));
			}

			System.out.println(updatedflightBaskets.size());
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return updatedflightBaskets;
	}

	private static Map<String, String> readBraches() {
		Map<String, String> branches = new HashMap<>();
		Map<String, String> agencies = new HashMap<>();
		PreparedStatement preStatement;
		try {

			preStatement = connTrevoo.prepareStatement("select Id, Name from Accounting2014.BranchAccount (NOLOCK)");
			ResultSet result = preStatement.executeQuery();
			while (result.next()) {
				agencies.put(result.getString("Id").trim(), result.getString("Name"));
			}

			preStatement = connTrevoo
					.prepareStatement("select Name, BranchId from Accounting2014.AgencyAccount (NOLOCK)");
			result = preStatement.executeQuery();
			while (result.next()) {
				branches.put(result.getString("Name"), agencies.get(result.getString("BranchId")));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return branches;
	}

	private static Map<String, String> readProviders() {
		Map<String, String> providers = new HashMap<>();
		PreparedStatement preStatement;
		try {
			preStatement = connTrevoo.prepareStatement(
					"select ProviderNo, ProviderName from Accounting2014.SupplierProviderMapping (NOLOCK)");
			ResultSet result = preStatement.executeQuery();
			while (result.next()) {
				providers.put(result.getString("ProviderNo").trim(), result.getString("ProviderName"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		providers.put("1852", "Amadeus Iran IR");
		return providers;
	}

	private static Map<Integer, String> getProductStatusses() {
		Map<Integer, String> productStatuses = new HashMap<Integer, String>();
		productStatuses.put(0, "NotLoaded");
		productStatuses.put(1, "Option");
		productStatuses.put(2, "SelectedAllocated");
		productStatuses.put(6, "RemovedFromFile");
		productStatuses.put(10, "PreBookingReservation");
		productStatuses.put(11, "Reservation");
		productStatuses.put(12, "Sales");
		productStatuses.put(16, "Cancelled");
		productStatuses.put(17, "Expired");
		productStatuses.put(18, "Refunding");
		productStatuses.put(19, "Refunded");
		productStatuses.put(20, "PartialRefunding");
		productStatuses.put(21, "PartialRefunded");

		return productStatuses;

	}

	private static Map<String, HashMap<String, String>> readBusinesses() {
		Map<String, HashMap<String, String>> businesses = new HashMap<String, HashMap<String, String>>();
		HashMap<String, String> bDetails = new HashMap<String, String>();
		PreparedStatement preStatement;
		try {
			preStatement = connTrevoo.prepareStatement(
					"select Id, Level, Name, SuperBusinessId, Location_Detail, Location_District,Location_Street,Location_Neighborhoods, Location_City, Location_Country, AgencyType, Status_IfActive, Status_IfForbidden, Activation_TransactionDate,GroupId from Accounts.Businesses (NOLOCK) where Level<4");
			ResultSet result = preStatement.executeQuery();
			while (result.next()) {
				bDetails = new HashMap<String, String>();
				bDetails.put("Name", result.getString("Name").trim());
				bDetails.put("Level", result.getString("Level"));
				bDetails.put("SuperBusinessId", result.getString("SuperBusinessId"));
				bDetails.put("Location_District", result.getString("Location_District").trim());
				bDetails.put("Location_Detail", result.getString("Location_Detail").trim());
				String street = "";
				if (result.getString("Location_Street") != null)
					street = result.getString("Location_Street").trim();
				bDetails.put("Location_Street", street);
				String neighborhood = "";
				if (result.getString("Location_Neighborhoods") != null)
					neighborhood = result.getString("Location_Neighborhoods").trim();
				bDetails.put("Location_Neighborhoods", neighborhood);
				bDetails.put("Location_City", result.getString("Location_City").trim());
				bDetails.put("Location_Country", result.getString("Location_Country").trim());
				bDetails.put("AgencyType", result.getString("AgencyType"));
				bDetails.put("Status_IfActive", result.getString("Status_IfActive"));
				bDetails.put("Status_IfForbidden", result.getString("Status_IfForbidden"));
				bDetails.put("Activation_TransactionDate", result.getString("Activation_TransactionDate"));
				if (result.getString("GroupId") != null)
					bDetails.put("GroupId", businessGroups.get(Integer.parseInt(result.getString("GroupId"))));
				businesses.put(result.getString("Id").trim(), bDetails);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return businesses;
	}

	private static Map<String, String> readAirlines() {
		Map<String, String> airlines = new HashMap<>();
		PreparedStatement preStatement;
		try {
			preStatement = connBiletBank.prepareStatement("select AirlineCode, AirlineName from air.Airlines (NOLOCK)");
			ResultSet result = preStatement.executeQuery();
			while (result.next()) {
				airlines.put(result.getString("AirlineCode").trim(), result.getString("AirlineName"));
			}
			airlines.put("THY", "Turkish Airlines");
			airlines.put("OHX", "Onur Air");
			airlines.put("CAI", "Corendon");
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return airlines;
	}

	private static Map<String, String> readAirportCities() {
		PreparedStatement preStatement;
		Map<String, String> airportCities = new HashMap<>();
		try {
			preStatement = connBiletBank.prepareStatement("select AirportCode, CityName from [Biletbank].air.Airports");
			ResultSet result = preStatement.executeQuery();

			while (result.next()) {
				airportCities.put(result.getString("AirportCode"), result.getString("CityName"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return airportCities;
	}

	private static Map<String, String> readAirportContinents() {
		PreparedStatement preStatement;
		Map<String, String> airportContinents = new HashMap<>();
		try {
			preStatement = connBiletBank.prepareStatement(
					"SELECT AirportCode,Continent FROM [Biletbank].[air].[Airports] a join air.Countries b on a.CountryCode=b.CountryCode ");
			ResultSet result = preStatement.executeQuery();

			while (result.next()) {
				airportContinents.put(result.getString("AirportCode"), result.getString("Continent"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return airportContinents;
	}

	private static Map<String, String> readAirportCountries() {
		PreparedStatement preStatement;
		Map<String, String> airportCities = new HashMap<>();
		try {
			preStatement = connBiletBank
					.prepareStatement("select AirportCode, CountryCode from [Biletbank].air.Airports");
			ResultSet result = preStatement.executeQuery();

			while (result.next()) {
				airportCities.put(result.getString("AirportCode"), result.getString("CountryCode"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return airportCities;
	}

	private static Map<String, String> readAirportCountryNames() {
		PreparedStatement preStatement;
		Map<String, String> airportCountryNames = new HashMap<>();
		try {
			preStatement = connBiletBank
					.prepareStatement("select AirportCode, CountryName from [Biletbank].air.Airports");
			ResultSet result = preStatement.executeQuery();

			while (result.next()) {
				airportCountryNames.put(result.getString("AirportCode"), result.getString("CountryName"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return airportCountryNames;
	}

	private static void update(String[] hundredbaskets) {

		StringBuilder basketIds = basketIdBuilder(hundredbaskets);
		try {

			PreparedStatement preStatementBaskets = connTrevoo.prepareStatement(
					"select * from Sale.ShoppingFiles (NOLOCK) where Id in (" + basketIds.toString() + ")",
					ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
			resultBaskets = preStatementBaskets.executeQuery();
			resultBaskets.beforeFirst();

			PreparedStatement preStatementProducts = connTrevoo.prepareStatement(
					"select * from Sale.ShoppingFile_Products (NOLOCK) where ShoppingFileId in (" + basketIds.toString()
							+ ") and ProductType = 1 and Status in (12,18,19,20,21) and BookingCode!='*MAHSUP'",
					ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
			resultProducts = preStatementProducts.executeQuery();

			PreparedStatement preStatementTickets = connTrevoo
					.prepareStatement(
							"select * from Sale.ShoppingFile_ProductItems (NOLOCK) where ShoppingFileId in ("
									+ basketIds.toString() + ")",
							ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
			resultTickets = preStatementTickets.executeQuery();

			PreparedStatement preStatementSegments = connTrevoo.prepareStatement(
					"select * from Sale.ShoppingFile_FlightSegments (NOLOCK) where ShoppingFileId in ("
							+ basketIds.toString() + ") order by DepartureDay,DepartureTime",
					ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
			resultSegments = preStatementSegments.executeQuery();

			PreparedStatement preStatementPassengers = connTrevoo
					.prepareStatement(
							"select ProductItemId, FirstName, LastName, Email, BirthDate from Sale.ShoppingFile_Passengers P (NOLOCK) join Sale.ShoppingFile_PaxReferences PR (NOLOCK) On P.Id=PR.PassengerId where P.ShoppingFileId in ("
									+ basketIds.toString() + ")",
							ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
			resultPassengers = preStatementPassengers.executeQuery();

			PreparedStatement preStatementCommissions = connTrevoo
					.prepareStatement(
							"select ShoppingFileId, ProductItemId, BusinessId, Selling_Amount, Ref_SfCode from Sale.ShoppingFile_Commissions (NOLOCK) where ShoppingFileId in ("
									+ basketIds.toString() + ")",
							ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
			resultCommissions = preStatementCommissions.executeQuery();

			PreparedStatement preStatementPayments = connTrevoo.prepareStatement(
					"select ShoppingFileId, Selling_AmountOfInterest, Selling_AmountOfInterest_Credit, CC_InstallmentCount, CC_CardHolder, PaymentType, CC_CardNumber from Sale.ShoppingFile_Payments (NOLOCK) where ShoppingFileId in ("
							+ basketIds.toString() + ") and LastCommand in ('PSTH','CRDT') ",
					ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
			resultPayments = preStatementPayments.executeQuery();

			PreparedStatement preStatementAgencyTransactions = connTrevoo
					.prepareStatement(
							"select PI.ShoppingFileId, PI.Id, TransactionAmount/Amount as rate, AT.Currency from Trevoo.Sale.ShoppingFile_ProductItems (NOLOCK) PI join Trevoo.Accounting.AgencyAccountTransaction (nolock)  AT on PI.Id=AT.ProductItemId where Currency!='TRY' and ShoppingFileId in ("
									+ basketIds.toString() + ")",
							ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
			// System.out.println(preStatementAgencyTransactions);
			resultAgencyAccountTransaction = preStatementAgencyTransactions.executeQuery();

			PreparedStatement preStatementAmadeusTaxYQ = connTrevoo.prepareStatement(
					"SELECT EntityId, sum(case when Name='AmadeusTaxYQ' then convert(float,replace(Value,',','.')) else 0.0 end) as sales,"
							+ "sum(convert(float,replace(Value,',','.'))) as total FROM [Trevoo].[Sale].[ShoppingFile_FreeSmallData] (nolock)"
							+ "where Name in ('AmadeusTaxYQ','RefundAmadeusTaxYQ') and Value<>'TRY' and ShoppingFileId in ("
							+ basketIds.toString() + ") group by EntityId",
					ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
			// System.out.println(preStatementAmadeusTaxYQ);
			resultAmadeusTaxYQ = preStatementAmadeusTaxYQ.executeQuery();

			PreparedStatement preStatementAmadeusTaxYR = connTrevoo.prepareStatement(
					"SELECT EntityId, sum(case when Name='AmadeusTaxYR' then convert(float,replace(Value,',','.')) else 0.0 end) as sales,"
							+ "sum(convert(float,replace(Value,',','.'))) as total FROM [Trevoo].[Sale].[ShoppingFile_FreeSmallData] (nolock) "
							+ "where Name in ('AmadeusTaxYR','RefundAmadeusTaxYR') and Value<>'TRY' and ShoppingFileId in ("
							+ basketIds.toString() + ") group by EntityId",
					ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
			// System.out.println(preStatementAmadeusTaxYR);
			resultAmadeusTaxYR = preStatementAmadeusTaxYR.executeQuery();

			createBaskets();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private static void createBaskets() {
		basket = new Document();
		Integer agencyId;
		try {
			while (resultBaskets.next()) {
				basket.clear();
				String id = resultBaskets.getString("Id");
				basket.put("_id", id);
				// System.out.println(id);
				agencyId = resultBaskets.getInt("Customer_BusinessId");
				if (agencyId != null) {
					getBusiness(agencyId);
				}

				if (resultBaskets.getObject("Customer_UserId") != null) {
					basket.put("CustomerUserId", resultBaskets.getInt("Customer_UserId"));
					basket.put("CustomerUserName", customerUsers.get(resultBaskets.getInt("Customer_UserId")));
				}

				Timestamp date = resultBaskets.getTimestamp("CreationDate");
				if (date != null) {
					Timestamp later = new Timestamp(date.getTime() + (1000 * 60 * 60 * TIME_DIFFERENCE));
					basket.put("CreationDate", format.parse(later.toString()));
					@SuppressWarnings("deprecation")
					int quarter = (date.getMonth() / 3) + 1;
					basket.put("Quarter", "Q" + quarter);
				}

				date = resultBaskets.getTimestamp("T_LastTransactionDate");
				if (date != null) {
					Timestamp later = new Timestamp(date.getTime() + (1000 * 60 * 60 * TIME_DIFFERENCE));
					basket.put("LastTransactionDate", format.parse(later.toString()));
				}

				if (resultBaskets.getObject("SellingCurrency") != null)
					basket.put("SellingCurrency", resultBaskets.getObject("SellingCurrency"));
				if (resultBaskets.getObject("If_Finalized") != null)
					basket.put("IsFinalized", resultBaskets.getBoolean("If_Finalized"));

				getTripType(id);
				getProducts(id, resultBaskets.getTimestamp("CreationDate"), agencyId);

				try {
					collection.insertOne(basket);
					insertCount++;

				} catch (MongoWriteException dk) {
					updateCount++;
					collection.replaceOne(new Document("_id", basket.getString("_id")), basket);
				}

			}
		} catch (JSONException | SQLException | ParseException e) {
			e.printStackTrace();
		}

	}

	private static void getBusiness(Integer businessId) throws SQLException, ParseException {
		Document business = new Document();
		Integer superBusinessId = null;

		HashMap<String, String> bDetails = businesses.get(businessId.toString());
		business.put("CustomerBusinessId", businessId);
		try {
			business.put("CustomerBusinessName", bDetails.get("Name"));

			if (branches.get(bDetails.get("Name")) == null)
				business.put("Branch", "BiletBank");
			else
				business.put("Branch", branches.get(bDetails.get("Name")));
			superBusinessId = Integer.parseInt(bDetails.get("SuperBusinessId"));
			business.put("CustomerSuperBusinessId", superBusinessId);
			business.put("District", bDetails.get("Location_District"));
			business.put("Location_Detail", bDetails.get("Location_Detail"));
			business.put("Street", bDetails.get("Location_Street"));
			business.put("Neighborhood", bDetails.get("Location_Neighborhoods"));
			business.put("City", bDetails.get("Location_City"));
			business.put("Country", bDetails.get("Location_Country"));
			business.put("Type", bDetails.get("AgencyType"));
			business.put("Level", bDetails.get("Level"));
			business.put("GroupId", bDetails.get("GroupId"));
			business.put("ActivationDate", bDetails.get("Activation_TransactionDate"));

			if (bDetails.get("Status_IfForbidden").equals("1")) {
				business.put("Status", "Yasakl�");
			} else if (bDetails.get("Status_IfActive").equals("1")) {
				business.put("Status", "Aktif");
			} else {
				business.put("Status", "Pasif");
			}

			bDetails = businesses.get(superBusinessId.toString());
			business.put("CustomerSuperBusinessName", bDetails.get("Name"));
		} catch (NullPointerException e) {
			System.err.println("No Such Business Exists" + basket.get("_id"));
		}
		basket.put("Business", business);
	}

	private static void getProducts(String id, Timestamp creationDate, Integer agencyId) throws SQLException {
		Timestamp sellingDate = null;
		List<Document> products = new ArrayList<>();

		new ArrayList<>();
		try {
			while (resultProducts.next()) {

				if (resultProducts.getString("ShoppingFileId").equals(id)
						&& resultProducts.getInt("ProductType") == 1) {

					product = new Document();
					product.put("productId", resultProducts.getString("Id"));
					product.put("TripType", resultProducts.getString("Flight_TripType"));
					if (resultProducts.getObject("ProductType") != null)
						product.put("ProductType", resultProducts.getInt("ProductType"));
					if (resultProducts.getObject("ProviderId") != null) {
						product.put("ProviderId", resultProducts.getInt("ProviderId"));
						product.put("ProviderName", providers.get(resultProducts.getObject("ProviderId").toString()));
						if (resultProducts.getString("ext_FreeData_Detail") != null
								&& resultProducts.getString("ext_FreeData_Detail").equals("Light-Ticketing")) {
							product.put("ProviderName", "Amadeus LT");
						}
					}

					String VATFlag = resultProducts.getString("VATFlag");
					if (resultProducts.getObject("VATFlag") != null)
						product.put("VATFlag", resultProducts.getString("VATFlag"));
					if (resultProducts.getObject("BookingCode") != null)
						product.put("PNR", resultProducts.getString("BookingCode").trim());
					resultProducts.getString("BookingCode");
					Timestamp d = resultProducts.getTimestamp("CreationDate");
					Timestamp later;
					if (d != null) {
						later = new Timestamp(d.getTime() + (1000 * 60 * 60 * TIME_DIFFERENCE));
						product.put("CreationDate", format.parse(later.toString()));
					}

					d = resultProducts.getTimestamp("PrebookingDate");
					if (d != null) {
						later = new Timestamp(d.getTime() + (1000 * 60 * 60 * TIME_DIFFERENCE));
						product.put("PrebookingDate", format.parse(later.toString()));
					}

					d = resultProducts.getTimestamp("ReservationDate");
					if (d != null) {
						later = new Timestamp(d.getTime() + (1000 * 60 * 60 * TIME_DIFFERENCE));
						product.put("ReservationDate", format.parse(later.toString()));
					}

					d = resultProducts.getTimestamp("SellingDate");
					if (d != null) {
						later = new Timestamp(d.getTime() + (1000 * 60 * 60 * TIME_DIFFERENCE));
						sellingDate = new Timestamp(d.getTime() + (1000 * 60 * 60 * TIME_DIFFERENCE));
					}

					d = resultProducts.getTimestamp("CancelDate");
					if (d != null) {
						later = new Timestamp(d.getTime() + (1000 * 60 * 60 * TIME_DIFFERENCE));
						product.put("CancelDate", format.parse(later.toString()));

					}

					if (resultProducts.getObject("Status") != null)
						product.put("Status", productStatuses.get(resultProducts.getInt("Status")));

					if (sellingDate == null) {
						System.out.println(creationDate);
						sellingDate = new Timestamp(creationDate.getTime() + (1000 * 60 * 60 * TIME_DIFFERENCE));
					}
					// Document fareclassinfo = getFareClassInfo(id,
					// resultProducts.getString("Id"), VATFlag);
					// getTickets(id, resultProducts.getString("Id"), VATFlag,
					// sellingDate,
					// resultProducts.getInt("ProviderId"), agencyId,
					// fareclassinfo);
					getTickets(id, resultProducts.getString("Id"), VATFlag, sellingDate,
							resultProducts.getInt("ProviderId"), agencyId);
					getSegments(new ArrayList<Document>(), id, resultProducts.getString("Id"));

					if (sellingDate != null) {
						products.add(product);
					}

				}
			}

			basket.put("Products", products);
			resultProducts.beforeFirst();
		} catch (JSONException | SQLException | ParseException e) {
			e.printStackTrace();
		}

	}

	private static Document getFareClassInfo(String id, String productid, String vATFlag) {
		Document fareinfo = new Document();
		int segcount = 0;
		try {
			while (resultSegments.next()) {

				if (resultSegments.getString("ShoppingFileId").equals(id)
						&& resultSegments.getString("ProductId").equals(productid)
						&& !resultSegments.getString("DepartureDay").equals("0001-01-01")) {
					// System.out.println(vATFlag);
					segment = new Document();
					segment.put("SegmentId", resultSegments.getString("Id"));
					try {
						if (vATFlag.equals("INT")
								&& airportCountries.get(resultSegments.getObject("Origin")).equals("TR")
								&& airportCountries.get(resultSegments.getObject("Destination")).equals("TR")) {
							continue;
						} else {
							if (resultSegments.getObject("Origin") != null
									&& resultSegments.getObject("OD_Origin") != null) {
								// if
								// (resultSegments.getObject("Origin").equals(resultSegments.getObject("OD_Origin")))
								// {
								segcount++;
								fareinfo.put("Origin" + segcount, resultSegments.getString("Origin"));
								fareinfo.put("OriginCity" + segcount,
										airportCities.get(resultSegments.getString("Origin")));
								fareinfo.put("OD_Origin" + segcount, resultSegments.getString("OD_Origin"));
								fareinfo.put("OD_OriginCity" + segcount,
										airportCities.get(resultSegments.getString("OD_Origin")));
								if (resultSegments.getObject("Destination") != null) {
									fareinfo.put("Destination" + segcount, resultSegments.getString("Destination"));
									fareinfo.put("DestinationCity" + segcount,
											airportCities.get(resultSegments.getString("Destination")));
								}

								if (resultSegments.getObject("OD_Destination") != null) {
									fareinfo.put("OD_Destination" + segcount,
											resultSegments.getString("OD_Destination"));
									fareinfo.put("OD_DestinationCity" + segcount,
											airportCities.get(resultSegments.getString("OD_Destination")));
								}
								if (resultSegments.getObject("DepartureDay") != null) {
									Timestamp d = resultSegments.getTimestamp("DepartureDay");
									if (d != null) {
										fareinfo.put("DepartureDate" + segcount, format.parse(d.toString()));
									}
								}
								if (resultSegments.getObject("FlightNo") != null)
									fareinfo.put("FlightNo" + segcount, resultSegments.getString("FlightNo"));
								if (resultSegments.getObject("BookingClass") != null)
									fareinfo.put("Class" + segcount, resultSegments.getString("BookingClass"));
							}

							// }
						}
					} catch (NullPointerException e) {

					}
				}

			}

			resultSegments.beforeFirst();
		} catch (JSONException | SQLException | ParseException e) {
			e.printStackTrace();
		}
		// System.out.println(fareinfo);
		return fareinfo;

	}

	private static List<Document> getSegments(List<Document> segments, String id, String productid) {
		List<String> marketingAirline = new ArrayList<String>();
		String routeSimple = "";
		String routeComplex = "";
		String classes = "";
		String Origin = null;
		String Destination = null;
		String departureDay = "";
		String departureTime = "";
		String dest = "";
		String marketingAirlineName = null;
		try {
			while (resultSegments.next()) {
				if (resultSegments.getString("ShoppingFileId").equals(id)
						&& resultSegments.getString("ProductId").equals(productid)
						&& !resultSegments.getString("DepartureDay").equals("0001-01-01")) {
					segment = new Document();
					segment.put("SegmentId", resultSegments.getString("Id"));
					if (resultSegments.getObject("SequenceInOD") != null)
						segment.put("SequenceInOD", resultSegments.getInt("SequenceInOD"));
					if (resultSegments.getObject("Origin") != null) {
						segment.put("Origin", resultSegments.getString("Origin"));
						segment.put("OriginCity", airportCities.get(resultSegments.getString("Origin")));
						segment.put("OriginCountry", airportCountryNames.get(resultSegments.getString("Origin")));
						segment.put("OriginContinent", airportContinents.get(resultSegments.getString("Origin")));
					}
					if (resultSegments.getObject("Destination") != null) {
						segment.put("Destination", resultSegments.getString("Destination"));
						segment.put("DestinationCity", airportCities.get(resultSegments.getString("Destination")));
						segment.put("DestinationCountry",
								airportCountryNames.get(resultSegments.getString("Destination")));
						segment.put("DestinationContinent",
								airportContinents.get(resultSegments.getString("Destination")));
					}

					if (resultSegments.getObject("OD_Origin") != null) {
						segment.put("OD_Origin", resultSegments.getString("OD_Origin"));
						segment.put("OD_OriginCity", airportCities.get(resultSegments.getString("OD_Origin")));
						segment.put("OD_OriginCountry", airportCountryNames.get(resultSegments.getString("OD_Origin")));
						segment.put("OD_OriginContinent", airportContinents.get(resultSegments.getString("OD_Origin")));
					}
					if (resultSegments.getObject("OD_Destination") != null) {
						segment.put("OD_Destination", resultSegments.getString("OD_Destination"));
						segment.put("OD_DestinationCity",
								airportCities.get(resultSegments.getString("OD_Destination")));
						segment.put("OD_DestinationCountry",
								airportCountryNames.get(resultSegments.getString("OD_Destination")));
						segment.put("OD_DestinationContinent",
								airportContinents.get(resultSegments.getString("OD_Destination")));
					}
					if (Origin == null) {
						Origin = resultSegments.getString("Origin");
						routeSimple += Origin + "/";
						routeComplex += Origin + "/";

					}

					Destination = resultSegments.getString("Destination");
					departureDay += resultSegments.getObject("DepartureDay").toString() + "/";
					departureTime += resultSegments.getObject("DepartureTime").toString() + "/";
					routeComplex += Destination + "/";

					classes += resultSegments.getString("BookingClass") + "/";
					if (!dest.equals(resultSegments.getString("Destination"))) {
						routeSimple += resultSegments.getString("Destination") + "/";
						dest = resultSegments.getString("Destination");
					}

					if (resultSegments.getObject("DepartureDay") != null) {
						segment.put("DepartureDay", resultSegments.getObject("DepartureDay").toString());
						Timestamp d = resultSegments.getTimestamp("DepartureDay");
						if (d != null) {
							segment.put("DepartureDate", format.parse(d.toString()));
						}
					}

					if (resultSegments.getObject("DepartureTime") != null) {
						segment.put("DepartureTime", resultSegments.getObject("DepartureTime").toString());
					}

					if (resultSegments.getObject("ArrivalDay") != null) {
						segment.put("ArrivalDay", resultSegments.getObject("ArrivalDay"));
					}

					if (resultSegments.getObject("ArrivalTime") != null) {
						segment.put("ArrivalTime", resultSegments.getObject("ArrivalTime"));
					}

					if (resultSegments.getObject("FlightNo") != null)
						segment.put("FlightNo", resultSegments.getString("FlightNo"));
					if (resultSegments.getObject("BookingClass") != null)
						segment.put("Class", resultSegments.getString("BookingClass"));
					if (resultSegments.getObject("MarketingAirline") != null) {
						segment.put("MarketingAirline", resultSegments.getString("MarketingAirline"));

						segment.put("MarketingAirlineName", airlines.get(resultSegments.getString("MarketingAirline")));
						if (marketingAirlineName == null)
							marketingAirlineName = airlines.get(resultSegments.getString("MarketingAirline"));
					}
					if (resultSegments.getObject("OperatingAirline") != null) {
						segment.put("OperatingAirline", resultSegments.getString("OperatingAirline"));
						segment.put("OperatingAirlineName", airlines.get(resultSegments.getString("OperatingAirline")));
					}
					if (resultSegments.getObject("FareBasis") != null)
						segment.put("FareBasis", resultSegments.getString("FareBasis"));
					if (resultSegments.getObject("FareType") != null)
						segment.put("FareType", resultSegments.getString("FareType"));

					marketingAirline.add(resultSegments.getString("MarketingAirline"));
					segments.add(segment);

				}

			}
			try {
				product.put("routeComplex", routeComplex.substring(0, routeComplex.length() - 1));
				product.put("Classes", classes.substring(0, classes.length() - 1));
				product.put("routeSimple", routeSimple.substring(0, routeSimple.length() - 1));
				product.put("DepartureDay", departureDay.substring(0, departureDay.length() - 1));
				product.put("DepartureTime", departureTime.substring(0, departureTime.length() - 1));

			} catch (StringIndexOutOfBoundsException e) {
				System.out.println(
						"there is a string out of bounds exception on route or departure day concatenation:" + id);
			}

			boolean allSame = true;
			if (marketingAirline.size() == 0) {
				System.err.println(resultBaskets.getString("Id"));
			}

			else {
				product.put("Airline", marketingAirlineName);
				String airline = marketingAirline.get(0);
				for (String s : marketingAirline) {
					if (!s.equals(airline)) {
						allSame = false;
					}
				}

				if (marketingAirline.size() > 1) {
					if (allSame) {
						if (!Destination.equals(Origin)) {
							basket.put("FlightType", "one way");
						} else {
							basket.put("FlightType", "round trip");
						}
					} else {
						basket.put("FlightType", "one way");
					}
				} else {
					basket.put("FlightType", "one way");
				}
			}
			product.put("Segments", segments);
			resultSegments.beforeFirst();
		} catch (JSONException | SQLException | ParseException e) {
			e.printStackTrace();
		}
		return segments;

	}

	private static void getTripType(String id) {
		String Origin = null;
		String Destination;

		try {
			while (resultSegments.next()) {
				if (resultSegments.getString("ShoppingFileId").equals(id)
						&& !resultSegments.getString("DepartureDay").equals("0001-01-01")) {

					if (Origin == null) {
						Origin = airportCities.get(resultSegments.getString("OD_Origin"));
					}
					Destination = airportCities.get(resultSegments.getString("OD_Destination"));
					if (Destination != null && Origin != null) {
						if (!Destination.equals(Origin)) {
							basket.put("MarketingFlightType", "one way");
						} else {
							basket.put("MarketingFlightType", "round trip");
						}
					}
				}
			}
			resultSegments.beforeFirst();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private static void getTickets(String id, String productid, String vATFlag, Timestamp sellingDate, int providerId,
			Integer agencyId) {
		Document date;
		List<Document> tickets = new ArrayList<>();
		List<Document> datess = new ArrayList<>();

		int ticketCount = 0;

		try {
			while (resultTickets.next()) {
				if (resultTickets.getString("ShoppingFileId").equals(id)) {
					ticketCount++;
				}
			}
			resultTickets.beforeFirst();
			while (resultTickets.next()) {
				datess = new ArrayList<>();
				if (resultTickets.getString("ShoppingFileId").equals(id)
						&& resultTickets.getString("ProductId").equals(productid)) {

					ticket = new Document();
					ticket.put("TicketId", resultTickets.getString("Id"));

					if (resultTickets.getObject("PassengerCount") != null)
						ticket.put("PassengerCount", resultTickets.getInt("PassengerCount"));
					if (resultTickets.getObject("Selling_Currency") != null)
						ticket.put("SellingCurrency", resultTickets.getString("Selling_Currency"));

					double baseFare = 0;
					if (resultTickets.getObject("Selling_BaseFare") != null) {
						baseFare = resultTickets.getDouble("Selling_BaseFare");
						ticket.put("BaseFare", baseFare);
					}
					double DFF = 0;
					if (resultTickets.getObject("Selling_BaseFare_AddOn") != null) {
						DFF = resultTickets.getDouble("Selling_BaseFare_AddOn");
						ticket.put("DFF", DFF);
					}

					getBFF(id, ticketCount, resultTickets.getString("Flight_TicketNumber"));
					double taxes = 0, YR = 0, VQ = 0, TXE = 0, AmadeusTaxYR = 0, AmadeusTaxYQ = 0, QC = 0,
							AmadeusTaxYRTotal = 0, AmadeusTaxYQTotal = 0;
					;
					double vergiler, vergilernet, vergilerTest, vergilernetTest;
					if (resultTickets.getObject("Selling_Taxes") != null) {
						taxes = resultTickets.getDouble("Selling_Taxes");
						ticket.put("Taxes", taxes);
					}

					if (resultAmadeusTaxYQ != null) {
						resultAmadeusTaxYQ.beforeFirst();
						while (resultAmadeusTaxYQ.next()) {

							if (resultAmadeusTaxYQ.getString("EntityId").equals(resultTickets.getString("Id"))) {
								AmadeusTaxYQ = resultAmadeusTaxYQ.getDouble("sales");
								ticket.put("AmadeusTaxYQ", AmadeusTaxYQ);
								AmadeusTaxYQTotal = resultAmadeusTaxYQ.getDouble("total");
								ticket.put("AmadeusTaxYQNet", AmadeusTaxYQTotal);
								// System.out.println("-YQ-" + AmadeusTaxYQ);
								// System.out.println("-YQNet-" +
								// AmadeusTaxYQTotal);
							}
						}

					}

					if (resultAmadeusTaxYR != null) {
						resultAmadeusTaxYR.beforeFirst();
						while (resultAmadeusTaxYR.next()) {
							// System.out.println("-TR-" +
							// resultAmadeusTaxYR.getString("EntityId"));
							if (resultAmadeusTaxYR.getString("EntityId").equals(resultTickets.getString("Id"))) {
								AmadeusTaxYR = resultAmadeusTaxYR.getDouble("sales");
								ticket.put("AmadeusTaxYR", AmadeusTaxYR);
								AmadeusTaxYRTotal = resultAmadeusTaxYR.getDouble("total");
								ticket.put("AmadeusTaxYRNet", AmadeusTaxYRTotal);
							}
						}

					}

					ticket.put("OderTax", taxes - AmadeusTaxYQ - AmadeusTaxYR);

					double SC = 0;
					if (resultTickets.getObject("Selling_ServiceFee") != null) {
						SC = resultTickets.getDouble("Selling_ServiceFee");
						ticket.put("SC", SC);
					}

					double providerSC = 0;
					if (resultTickets.getObject("Selling_ProviderServiceFee") != null) {
						providerSC = resultTickets.getDouble("Selling_ProviderServiceFee");
						// if (providerId == 1852) {
						// DFF += providerSC; //iran void i�lemi
						// providerSC = 0;
						// ticket.put("DFF", DFF);
						// }
						ticket.put("ProviderSC", providerSC);
					}

					if (resultTickets.getObject("Selling_VQ") != null) {
						VQ = resultTickets.getDouble("Selling_VQ");
						ticket.put("VQ", VQ);
					}

					if (resultTickets.getObject("Selling_TXE") != null) {
						TXE = resultTickets.getDouble("Selling_TXE");
						ticket.put("TXE", TXE);
					}
					if (resultTickets.getObject("Selling_QC") != null) {
						QC = resultTickets.getDouble("Selling_QC");
						ticket.put("QC", QC);
					}

					if (resultTickets.getObject("Selling_YR") != null) {
						YR = resultTickets.getDouble("Selling_YR");
						ticket.put("YR", YR + TXE);
					}
					if (resultTickets.getObject("Ext_Reissue_OldTicketNo") != null) {
						ticket.put("reissueOldTicketNumber", resultTickets.getString("Ext_Reissue_OldTicketNo").trim());
					}

					double payback = getPayback(resultTickets.getString("Id"));

					if (payback != 0) {
						ticket.put("Payback", payback);
					}
					double BaseFareNet, SCNet = SC;
					double taxesNET = 0, YRNET = 0, VQNET = 0, TXENET = 0, DFFNet, AgencyDFF;

					date = new Document();
					try {
						date.put("SellingDate", format.parse(sellingDate.toString()));
					} catch (NullPointerException e) {
					}
					datess.add(date);
					date = new Document();

					if (resultTickets.getObject("Refund_IsPerformed") != null
							&& resultTickets.getObject("Selling_BaseFare_Refund") != null) {
						ticket.put("BaseFareRefund", resultTickets.getDouble("Selling_BaseFare_Refund"));
						BaseFareNet = resultTickets.getDouble("Selling_BaseFare")
								- resultTickets.getDouble("Selling_BaseFare_Refund");
						ticket.put("BaseFareNet", round(BaseFareNet, 2));

						ticket.put("TaxesRefund", resultTickets.getDouble("Selling_Taxes_Refund"));
						taxesNET = resultTickets.getDouble("Selling_Taxes")
								- resultTickets.getDouble("Selling_Taxes_Refund");
						ticket.put("TaxesNet", round(taxesNET, 2));
						ticket.put("OderTaxNet", taxesNET - AmadeusTaxYQTotal - AmadeusTaxYRTotal);
						ticket.put("VQRefund", resultTickets.getDouble("Selling_VQ_Refund"));
						VQNET = resultTickets.getDouble("Selling_VQ") - resultTickets.getDouble("Selling_VQ_Refund");
						ticket.put("VQNet", round(VQNET, 2));

						ticket.put("TXERefund", resultTickets.getDouble("Selling_TXE_Refund"));
						TXENET = resultTickets.getDouble("Selling_TXE") - resultTickets.getDouble("Selling_TXE_Refund");
						ticket.put("TXENet", round(TXENET, 2));

						ticket.put("YRRefund", resultTickets.getDouble("Selling_YR_Refund")
								+ resultTickets.getDouble("Selling_TXE_Refund"));
						YRNET = resultTickets.getDouble("Selling_YR") - resultTickets.getDouble("Selling_YR_Refund");
						ticket.put("YRNet", round(YRNET + TXENET, 2));

						ticket.put("SCRefund", resultTickets.getDouble("Selling_ServiceFee_Refund"));
						SCNet = resultTickets.getDouble("Selling_ServiceFee")
								- resultTickets.getDouble("Selling_ServiceFee_Refund");
						ticket.put("SCNet", round(SCNet, 2));

						ticket.put("DFFRefund", resultTickets.getDouble("Selling_BaseFare_AddOn_Refund"));
						DFFNet = resultTickets.getDouble("Selling_BaseFare_AddOn")
								- resultTickets.getDouble("Selling_BaseFare_AddOn_Refund");
						DFFNet = DFFNet + resultTickets.getDouble("Selling_Refund_Charge")
								+ resultTickets.getDouble("Refund_AgencyDffCommRate")
										* resultTickets.getDouble("Refund_AgencyDffAmount");

						AgencyDFF = (1 - resultTickets.getDouble("Refund_AgencyDffCommRate"))
								* resultTickets.getDouble("Refund_AgencyDffAmount");
						ticket.put("DFFNet", round(DFFNet, 2));
						ticket.put("AgencyDFF", round(AgencyDFF, 2));

						ticket.put("Refund_IsPerformed", resultTickets.getInt("Refund_IsPerformed"));

						vergilernet = calculateTaxesRefund(VQNET, YRNET, taxesNET, TXENET, vATFlag, VQ);
						vergilernetTest = calculateTaxesTest(VQNET, YRNET, taxesNET, TXENET, vATFlag,
								isECN(id, productid), 0);
						ticket.put("VergilerNet", round(vergilernet, 2));
						ticket.put("VergilernetTest", round(vergilernetTest, 2));
						double ciro = BaseFareNet + vergilernet + SCNet + DFFNet + providerSC + AgencyDFF;
						double ciroTest = BaseFareNet + vergilernetTest + SCNet + DFFNet + providerSC + AgencyDFF;
						ticket.put("CiroNet", round(ciro, 2));
						ticket.put("CiroNetTest", round(ciroTest, 2));
						ticket.put("ProviderCiroNet", round((BaseFareNet + vergilernet + providerSC), 2));

						Timestamp d = resultTickets.getTimestamp("Refund_PerformedDate");
						if (d != null && resultTickets.getObject("Selling_BaseFare_Refund") != null) {
							Timestamp later = new Timestamp(d.getTime() + (1000 * 60 * 60 * TIME_DIFFERENCE));
							date.put("CancelDate", format.parse(later.toString()));
						}
						if (!date.isEmpty())
							datess.add(date);
					}

					vergiler = calculateTaxes(VQ, YR, taxes, TXE, vATFlag);
					vergilerTest = calculateTaxesTest(VQ, YR, taxes, TXE, vATFlag, isECN(id, productid), QC);

					ticket.put("Vergiler", round(vergiler, 2));
					ticket.put("VergilerTest", round(vergilerTest, 2));

					ticket.put("Ciro", round((baseFare + vergiler + SC + DFF + providerSC), 2));

					ticket.put("CiroTest", round((baseFare + vergilerTest + SC + DFF + providerSC), 2));
					ticket.put("ProviderCiro", round((baseFare + vergiler + providerSC), 2));

					if (resultTickets.getObject("Selling_Refund_Charge") != null)
						ticket.put("RefundCharge", round(resultTickets.getDouble("Selling_Refund_Charge"), 2));

					if (resultTickets.getObject("Flight_TicketNumber") != null) {
						ticket.put("TicketNumber", resultTickets.getString("Flight_TicketNumber").trim());
						// if (troyaAgencies.contains(agencyId)) {
						if (airlineCommissions.get(resultTickets.getString("Flight_TicketNumber").trim()) != null)
							ticket.put("TroyaDownloadPaymentType", airlineCommissions
									.get(resultTickets.getString("Flight_TicketNumber").trim()).get("PaymentType"));
						if (airlineCommissions.get(resultTickets.getString("Flight_TicketNumber").trim()) != null)
							ticket.put("TroyaDownloadAirlineCommission",
									airlineCommissions.get(resultTickets.getString("Flight_TicketNumber").trim())
											.get("airlineCommission"));
						// }
						try {
							String threedgt = resultTickets.getString("Flight_TicketNumber").substring(0, 3);
							if (airline3dgtCodes.get(threedgt) != null)
								ticket.put("Airline", airline3dgtCodes.get(threedgt));
						} catch (StringIndexOutOfBoundsException e) {
							System.out.println(resultTickets.getString("Flight_TicketNumber"));
						}
					}

					if (resultTickets.getObject("Flight_Baggage") != null)
						ticket.put("Baggage", resultTickets.getString("Flight_Baggage"));

					getCommissions(id, resultTickets.getString("Id"));
					getPassenger(resultTickets.getString("Id"));
					ticket.put("Dates", datess);

					// foreign currency transaction
					String currency = null;
					Double xRate = null;
					while (resultAgencyAccountTransaction.next()) {
						if (resultAgencyAccountTransaction.getString("ShoppingFileId").equals(id)
								&& resultAgencyAccountTransaction.getString("Id")
										.equals(resultTickets.getString("Id"))) {
							xRate = resultAgencyAccountTransaction.getDouble("rate");
							currency = resultAgencyAccountTransaction.getString("Currency");

						}
					}
					resultAgencyAccountTransaction.beforeFirst();
					ticket.put("XRate", xRate);
					ticket.put("Currency", currency);
					// ticket.put("FareClassInfo", fareclassinfo);
					tickets.add(ticket);

				}

			}

			product.put("Tickets", tickets);
			resultTickets.beforeFirst();
		} catch (JSONException | SQLException | ParseException e) {
			e.printStackTrace();
		}

	}

	private static boolean isECN(String id, String productid) {
		boolean isErcan = false;
		try {
			while (resultSegments.next()) {
				if (resultSegments.getString("ShoppingFileId").equals(id)
						&& resultSegments.getString("ProductId").equals(productid)
						&& !resultSegments.getString("DepartureDay").equals("0001-01-01")) {
					if (resultSegments.getObject("Origin").equals("ECN")) {
						isErcan = true;
					} else if (resultSegments.getObject("Destination").equals("ECN")) {
						isErcan = true;
					}
				}
			}
			resultSegments.beforeFirst();
		} catch (JSONException | SQLException e) {
			e.printStackTrace();
		}
		return isErcan;
	}

	private static Map<String, HashMap<String, String>> readComm() {
		Map<String, HashMap<String, String>> comissions = new HashMap<String, HashMap<String, String>>();
		HashMap<String, String> rDetails = new HashMap<String, String>();
		PreparedStatement preStatement;
		try {
			preStatement = connPetproApp_Data2013.prepareStatement(
					"select TicketNumber,max(PaymentType) as PaymentType,sum(convert(float,airlineCommission)) as  airlineCommission from dbo.TroyaDownload (NOLOCK) "
							+ "where AirlineCommission is not null group by TicketNumber");
			ResultSet result = preStatement.executeQuery();
			while (result.next()) {
				rDetails = new HashMap<String, String>();
				rDetails.put("PaymentType", result.getString("PaymentType").trim());
				rDetails.put("airlineCommission", result.getString("airlineCommission"));
				comissions.put(result.getString("TicketNumber").trim(), rDetails);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return comissions;

	}

	private static double getPayback(String ticketid) {
		PreparedStatement preStatementPayback;
		ResultSet resultPayback;
		double payback = 0;
		try {
			preStatementPayback = connTrevoo.prepareStatement(
					"select SUM(Comm_Debt) as payback from [Trevoo].[Accounting2014].[BookingItemLine] (nolock) where BookingItemId='"
							+ ticketid + "' and IsAgencyPayback=1  and Reason='SELL'",
					ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
			resultPayback = preStatementPayback.executeQuery();
			while (resultPayback.next()) {
				if (resultPayback.getObject("Payback") != null)
					payback = resultPayback.getDouble("Payback");
				else
					payback = 0;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return payback;
	}

	private static Object round(double value, int places) {

		if (places < 0)
			throw new IllegalArgumentException();

		long factor = (long) Math.pow(10, places);
		value = value * factor;
		long tmp = Math.round(value);
		return (double) tmp / factor;

	}

	private static double calculateTaxesRefund(double vQ, double yR, double taxes, double tXE, String vATFlag,
			double vQ2) {
		double vergiler = 0;
		if (vATFlag.equals("DOM")) {
			if (vQ2 != 0) {
				vergiler = vQ + yR + tXE;
			} else {
				vergiler = taxes;
			}
		} else {
			vergiler = taxes;
		}

		return vergiler;
	}

	private static double calculateTaxes(double vQ, double yR, double taxes, double tXE, String vATFlag) {
		double vergiler = 0;
		if (vATFlag.equals("DOM")) {
			if (vQ != 0) {
				vergiler = vQ + yR + tXE;
			} else {
				vergiler = taxes;
			}
		} else {
			vergiler = taxes;
		}

		return vergiler;
	}

	private static double calculateTaxesTest(double VQ, double YR, double taxes, double TXE, String vATFlag,
			boolean isECN, double QC) {
		double vergiler = 0;
		if (vATFlag.equals("INT") || isECN) {
			if (TXE >= QC) {
				vergiler = taxes - TXE;
			} else {
				vergiler = taxes - QC;
			}
		} else {
			if (TXE >= QC) {
				vergiler = YR + VQ + TXE;
			} else {
				vergiler = YR + VQ + QC;
			}
		}

		return vergiler;
	}

	private static void getBFF(String id, int ticketCount, String tnumber) {
		double bff = 0;
		double bffRefund = 0;
		Integer installmentCount = null;
		String PaymentType = null;
		String CCCardNumber = null;
		String CC_CardHolder = null;
		try {
			while (resultPayments.next()) {
				installmentCount = null;
				if (resultPayments.getString("ShoppingFileId").equals(id)) {
					bff += resultPayments.getDouble("Selling_AmountOfInterest");
					// System.out.println(bff);
					bffRefund += resultPayments.getDouble("Selling_AmountOfInterest_Credit");
					if (resultPayments.getObject("CC_InstallmentCount") != null) {
						installmentCount = resultPayments.getInt("CC_InstallmentCount");
					}
					if (resultPayments.getObject("PaymentType") != null) {
						PaymentType = resultPayments.getString("PaymentType");
					}
					if (resultPayments.getObject("CC_CardNumber") != null) {
						CCCardNumber = resultPayments.getString("CC_CardNumber");
					}
					if (resultPayments.getObject("CC_CardHolder") != null) {
						CC_CardHolder = resultPayments.getString("CC_CardHolder").split("/")[0];
					}
					// if (resultPayments.getObject("CC_CardHolder") != null) {
					//
					// try {
					// if (PaymentType.equals("CC")) {
					// ticket.put("PaymentType", PaymentType);
					// ticket.put("CCCardNumber", CCCardNumber);
					// ticket.put("CC_CardHolder", CC_CardHolder);
					// } else {
					// ticketnumber =
					// resultPayments.getString("CC_CardHolder").split("/")[1].trim();
					// if (tnumber.equals(ticketnumber)) {
					// ticket.put("PaymentType", PaymentType);
					// ticket.put("CCCardNumber", CCCardNumber);
					// ticket.put("CC_CardHolder", CC_CardHolder);
					// }
					// }
					// } catch (ArrayIndexOutOfBoundsException e) {
					// System.out.println("there is a problem with payment
					// type:" + id);
					// }
					//
					// }
					// System.out.println(installmentCount);

				}
			}
		} catch (JSONException | SQLException e) {
			e.printStackTrace();
		}
		try {
			resultPayments.beforeFirst();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		double bffNet = bff - bffRefund;
		ticket.put("BFF", round(bff / ticketCount, 2));
		ticket.put("BFFrefund", round(bffRefund / ticketCount, 2));
		ticket.put("BFFNet", round(bffNet / ticketCount, 2));
		ticket.put("installmentCount", installmentCount);
		ticket.put("PaymentType", PaymentType);
		ticket.put("CCCardNumber", CCCardNumber);
		ticket.put("CC_CardHolder", CC_CardHolder);

	}

	private static void getCommissions(String id, String ticketid) {
		List<Document> commissions = new ArrayList<>();
		Map<Integer, Double> comm = new HashMap<Integer, Double>();
		Map<Integer, String> fare = new HashMap<Integer, String>();
		Integer business;
		double amount;
		String fareclass;
		try {
			while (resultCommissions.next()) {

				if (resultCommissions.getString("ShoppingFileId").equals(id)
						&& resultCommissions.getString("ProductItemId").equals(ticketid)) {
					if (resultCommissions.getString("Ref_SfCode") != null
							&& resultCommissions.getString("Ref_SfCode").equals("DFF")) {
						continue;
					} else
						business = resultCommissions.getInt("BusinessId");
					amount = resultCommissions.getDouble("Selling_Amount");
					fareclass = resultCommissions.getString("Ref_SfCode");
					if (!comm.containsKey(business)) {
						comm.put(business, amount);
						fare.put(business, fareclass);
					} else {
						comm.put(business, amount + comm.get(business));
						fare.put(business, fareclass);
					}
				}
			}
			resultCommissions.beforeFirst();
		} catch (JSONException | SQLException e) {
			e.printStackTrace();
		}

		Document commission;
		for (Entry<Integer, Double> entry : comm.entrySet()) {
			commission = new Document();
			commission.put("FareClass", fare.get(entry.getKey()));
			commission.put("Business", (businesses.get(entry.getKey().toString()) == null) ? ""
					: businesses.get(entry.getKey().toString()).get("Name"));
			commission.put("Amount", entry.getValue());
			commission.put("Level", (businesses.get(entry.getKey().toString()) == null) ? ""
					: Integer.parseInt(businesses.get(entry.getKey().toString()).get("Level")));
			commissions.add(commission);
		}
		ticket.put("Commissions", commissions);
	}

	private static void getPassenger(String ticketId) {
		try {
			passenger = new Document();
			while (resultPassengers.next()) {
				if (resultPassengers.getString("ProductItemId").equals(ticketId)) {
					passenger.put("First Name", resultPassengers.getString("FirstName"));
					passenger.put("Last Name", resultPassengers.getString("LastName"));
					passenger.put("Birth Date", resultPassengers.getObject("BirthDate").toString());
					if (resultPassengers.getObject("Email") != null)
						passenger.put("email", resultPassengers.getString("Email"));
				}
			}
			ticket.put("Passenger", passenger);
			resultPassengers.beforeFirst();
		} catch (JSONException | SQLException e) {
			e.printStackTrace();
		}

	}

	private static StringBuilder basketIdBuilder(String[] hundredbaskets) {
		StringBuilder builder = new StringBuilder();
		builder.append("'");
		for (int i = 0; i < hundredbaskets.length; i++) {
			if (hundredbaskets[i] != null) {
				builder.append(hundredbaskets[i]);
				builder.append("','");
			}
		}
		builder.deleteCharAt(builder.length() - 1);
		builder.deleteCharAt(builder.length() - 1);

		return builder;
	}

	private static StringBuilder basketIdBuilder(Object[] hundredbaskets) {
		StringBuilder builder = new StringBuilder();
		builder.append("'");
		for (int i = 0; i < hundredbaskets.length; i++) {
			if (hundredbaskets[i] != null) {
				builder.append(hundredbaskets[i].toString());
				builder.append("','");
			}
		}
		builder.deleteCharAt(builder.length() - 1);
		builder.deleteCharAt(builder.length() - 1);

		return builder;
	}

	private static Set<String> readNonExistingBasketIds(String lastUpdate, String updateUntil) {
		Set<String> allBaskets = new HashSet<>();

		allBaskets = readAllBasketIds(lastUpdate, updateUntil);

		PreparedStatement preStatementRemoved;
		try {
			preStatementRemoved = connTrevoo.prepareStatement(
					"SELECT t1.ID FROM Sale.ShoppingFiles t1 LEFT JOIN [Sale].ShoppingFile_Products t2 ON t1.ID = t2.ShoppingFileId WHERE t2.ShoppingFileId IS NULL and dateadd(HOUR, 3, T_LastTransactionDate) >= '"
							+ lastUpdate + "' and dateadd(HOUR, 3, T_LastTransactionDate) < '" + updateUntil + "'",
					ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
			System.out.println(preStatementRemoved);
			ResultSet resultRemoved = preStatementRemoved.executeQuery();

			while (resultRemoved.next()) {
				allBaskets.add(resultRemoved.getString("Id"));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println("all baskets: " + allBaskets.size());
		return allBaskets;
	}

	private static Set<String> readAllBasketIdsInMongo() {
		Set<String> existingBaskets = new HashSet<>();
		MongoCollection<Document> collection = db.getCollection("baskets");

		MongoCursor<Document> cursor = collection.find().iterator();

		try {
			while (cursor.hasNext()) {
				existingBaskets.add(cursor.next().get("_id").toString());
			}
		} finally {
			cursor.close();
		}

		return existingBaskets;
	}

	private static Set<String> readAllBasketIds(String lastUpdate, String updateUntil) {
		Set<String> updatedBaskets = new HashSet<>();
		PreparedStatement preStatement;
		try {
			preStatement = connTrevoo.prepareStatement(
					"select distinct ShoppingFileId from Sale.ShoppingFile_Products (NOLOCK) where ProductType=1 and Status in (12,18,19,20,21) and BookingCode!='*MAHSUP' and dateadd(HOUR, 3, CreationDate) >= '"
							+ lastUpdate + "' and dateadd(HOUR, 3, CreationDate) < '" + updateUntil + "'");

			System.out.println(preStatement);
			ResultSet result = preStatement.executeQuery();
			while (result.next()) {
				updatedBaskets.add(result.getString("ShoppingFileId"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return updatedBaskets;
	}

	private static Connection connect(String dbName, String IP, String User, String PassWord) {
		Connection conn = null;
		try {
			Class.forName("net.sourceforge.jtds.jdbc.Driver");
			conn = DriverManager.getConnection("jdbc:jtds:sqlserver://" + IP + "/" + dbName, User, PassWord);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return conn;
	}

	private static Connection connect2(String dbName, String port, String IP, String User, String PassWord) {
		Connection conn = null;
		try {
			Class.forName("net.sourceforge.jtds.jdbc.Driver");
			conn = DriverManager.getConnection("jdbc:jtds:sqlserver://" + IP + ":" + port + "/" + dbName, User,
					PassWord);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return conn;
	}

}
