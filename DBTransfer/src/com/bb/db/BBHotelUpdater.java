package com.bb.db;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;

import org.bson.Document;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONException;

import com.mongodb.BasicDBObject;
import com.mongodb.Block;
import com.mongodb.MongoClient;
import com.mongodb.MongoWriteException;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;

import util.PropertyReader;

public class BBHotelUpdater {

	private static final String CONFIG_PROPERTIES = "config_bb.properties";
	private static final int TIME_DIFFERENCE = 3;
	static DateFormat format;
	static Connection connTrevoo;
	static Connection connBiletBank;
	static Properties prop;
	static MongoDatabase db;
	static int insertCount = 0;
	static int updateCount = 0;
	static MongoCollection<Document> collection;
	static ResultSet resultBaskets;
	static ResultSet resultloc;
	static ResultSet resultProducts;
	static ResultSet resultCommissions;
	static ResultSet resultBusinesses;
	static ResultSet resultBookings;
	static ResultSet resultPayments;
	static ResultSet resultPassengers;
	static ResultSet resultTickets;

	static Document basket;
	static Document product;
	static Document ticket;
	static Document segment;
	static Document passenger;

	static Map<String, HashMap<String, String>> businesses;
	static Map<Integer, String> customerUsers;
	static Map<String, String> providers;
	static Map<String, String> branches;
	static Map<Integer, String> businessGroups;

	static Map<Integer, String> productStatuses;
	static long startTime;
	static int conj;

	public static void main(String[] args) {
		conj = 0;
		format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		prop = new Properties();
		prop = PropertyReader.getProperties(CONFIG_PROPERTIES);
		connTrevoo = connect(prop.getProperty("maindb"));
		connBiletBank = connect("BiletBank");

		String MongoIP = prop.getProperty("MONGO_IP");
		MongoClient mongoClient = new MongoClient(MongoIP, 27017);
		db = mongoClient.getDatabase(prop.getProperty("Mongo_DB_Name"));

		collection = db.getCollection(prop.getProperty("MONGO_COLLECTION_NAME_HOTEL"));

		Set<String> updatedBaskets = new HashSet<>();

		businesses = new HashMap<>();
		customerUsers = new HashMap<>();

		// System.out.println(airline3dgtCodes);
		branches = readBraches();
		businessGroups = readBusinessGroups();

		providers = readProviders();
		productStatuses = getProductStatusses();
		businesses = readBusinesses();
		customerUsers = readCustomerUsers();

		 updatedBaskets =
		 readUpdatedBasketIds(prop.get("lastUpdate").toString(),
		 prop.get("updateUntil").toString());

		// updatedBaskets = readWrongDatedBasketIds();
		updatedBaskets = readNonExistingBasketIds(prop.get("lastUpdate").toString(),
				prop.get("updateUntil").toString());
		// updatedBaskets.add("CD1FF267-1D8A-4C0C-8F32-E351AC6920C1");
		// updatedBaskets = readbyQuery();
		// updatedBaskets = readbySQLQuery();

		System.out.println(updatedBaskets.size() + " baskets will be updated/added");

		int batchCount = Integer.parseInt(prop.getProperty("BATCH_COUNT"));
		System.out.println(batchCount);
		int order = 0;
		int binCount = 0;
		String[] hundredbaskets = new String[batchCount];
		for (String basketId : updatedBaskets) {
			hundredbaskets[order] = basketId;
			order++;
			if (order == batchCount) {
				order = 0;
				binCount++;
				// if (binCount <= Integer.parseInt(args[0]))
				// continue;
				// else {
				update(hundredbaskets);
				hundredbaskets = new String[batchCount];
				// }
				System.out.println(
						(binCount * batchCount) + " baskets processed " + new Timestamp(System.currentTimeMillis()));

			}
		}
		if (hundredbaskets[0] != null)
			update(hundredbaskets);

		System.out.println("# baskets inserted:\t" + insertCount);
		System.out.println("# baskets updated:\t" + updateCount);

		// updatePropertiesFile();
		// updatePropertiesFileByMonth();
		// updatePropertiesFileByHour(Integer.parseInt(args[0]));

		mongoClient.close();
		close(connTrevoo);
	}

	private static Map<Integer, String> readBusinessGroups() {

		Map<Integer, String> groups = new HashMap<>();
		PreparedStatement preStatement;
		try {

			preStatement = connTrevoo.prepareStatement("select Id, Code from Accounts.BusinessGroups (NOLOCK)");
			ResultSet result = preStatement.executeQuery();
			while (result.next()) {
				groups.put(result.getInt("Id"), result.getString("Code"));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return groups;
	}

	private static Set<String> readbySQLQuery() {
		Set<String> updatedBaskets = new HashSet<>();
		PreparedStatement preStatement;
		try {
			preStatement = connTrevoo.prepareStatement(
					"SELECT distinct ShoppingFileId FROM [Trevoo].[Sale].ShoppingFile_Products (NOLOCK) where ProviderId in (1,2,7) and ProductType = 2 and CreationDate>'2015-01-01' and Status in (12,18,19,20,21)");
			System.err.println(preStatement);
			ResultSet result = preStatement.executeQuery();
			while (result.next()) {
				updatedBaskets.add(result.getString("ShoppingFileId"));
			}
			System.out.println(updatedBaskets.size());

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return updatedBaskets;
	}

	private static void updatePropertiesFileByHour(int i) {
		DateTimeFormatter fmt = DateTimeFormat.forPattern("YYYY-MM-dd HH:mm:ss");
		DateTime dt = fmt.parseDateTime(prop.getProperty("updateUntil"));

		dt = dt.plusMinutes(i * 30);
		OutputStream output = null;
		try {
			output = new FileOutputStream(CONFIG_PROPERTIES);

			prop.setProperty("lastUpdate", prop.getProperty("updateUntil"));
			prop.setProperty("updateUntil", fmt.print(dt));
			prop.store(output, null);

			output.close();
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private static Set<String> readbyQuery() {
		System.out.println("reading wrong dated basket ids");
		Set<String> invalidDatedBaskets = new HashSet<>();
		MongoCollection<Document> collection = db.getCollection("baskets");

		BasicDBObject inQuery = new BasicDBObject();

		inQuery.put("Products.Tickets.Airline", null);

		MongoCursor<Document> cursor = collection.find(inQuery).iterator();

		try {
			while (cursor.hasNext()) {
				invalidDatedBaskets.add(cursor.next().get("_id").toString());
			}
		} finally {
			cursor.close();
		}

		return invalidDatedBaskets;
	}

	private static Map<Integer, String> readCustomerUsers() {
		Map<Integer, String> users = new HashMap<>();
		PreparedStatement preStatement;
		try {
			preStatement = connTrevoo.prepareStatement("select Id, UserName from Accounts.Users (NOLOCK)");
			ResultSet result = preStatement.executeQuery();
			while (result.next()) {
				users.put(result.getInt("Id"), result.getString("Username").trim());
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return users;
	}

	private static Map<String, String> readAirline3dgtCodes() {
		final Map<String, String> airlinecodes = new HashMap<String, String>();
		FindIterable<Document> iterable = db.getCollection("airline3dgtcodes").find();

		iterable.forEach(new Block<Document>() {
			@Override
			public void apply(final Document document) {
				airlinecodes.put(document.getString("3dgtCode"), document.getString("_id"));
			}
		});

		return airlinecodes;
	}

	private static void close(Connection conn2) {
		try {
			connTrevoo.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	private static void updatePropertiesFile() {

		DateTimeFormatter fmt = DateTimeFormat.forPattern("YYYY-MM-dd HH:mm:ss");
		DateTime dt = fmt.parseDateTime(prop.getProperty("updateUntilHotel"));

		dt = dt.plusDays(1);
		OutputStream output = null;
		try {
			output = new FileOutputStream(CONFIG_PROPERTIES);

			prop.setProperty("lastUpdateHotel", prop.getProperty("updateUntilHotel"));
			prop.setProperty("updateUntilHotel", fmt.print(dt));
			prop.store(output, null);

			output.close();
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static void updatePropertiesFileByMonth() {

		DateTimeFormatter fmt = DateTimeFormat.forPattern("YYYY-MM-dd HH:mm:ss");
		DateTime dt = fmt.parseDateTime(prop.getProperty("updateUntil"));

		dt = dt.plusMonths(1);
		OutputStream output = null;
		try {
			output = new FileOutputStream(CONFIG_PROPERTIES);

			prop.setProperty("lastUpdate", prop.getProperty("updateUntil"));
			prop.setProperty("updateUntil", fmt.print(dt));
			prop.store(output, null);

			output.close();
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static Set<String> readUpdatedBasketIds(String lastUpdateDate, String updateUntil) {
		Set<String> updatedBaskets = new HashSet<>();
		Set<String> updatedflightBaskets = new HashSet<>();
		PreparedStatement preStatement;
		try {
			preStatement = connTrevoo.prepareStatement(
					"select distinct Id from Sale.ShoppingFiles (NOLOCK) where dateadd(HOUR, 3, T_LastTransactionDate) >='"
							+ lastUpdateDate + "' and dateadd(HOUR, 3, T_LastTransactionDate) <'" + updateUntil + "'");
			System.err.println(preStatement);
			ResultSet result = preStatement.executeQuery();
			while (result.next()) {
				updatedBaskets.add(result.getString("Id"));
			}
			System.out.println(updatedBaskets.size());
			StringBuilder basketIds = basketIdBuilder(updatedBaskets.toArray());

			PreparedStatement preStatementProducts = connTrevoo.prepareStatement(
					"select ShoppingFileId from Sale.ShoppingFile_Products (NOLOCK) where ShoppingFileId in ("
							+ basketIds.toString()
							+ ") and ProductType = 2 and Status in (12,18,19,20,21) and BookingCode!='*MAHSUP'",
					ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
			resultProducts = preStatementProducts.executeQuery();

			while (resultProducts.next()) {
				updatedflightBaskets.add(resultProducts.getString("ShoppingFileId"));
			}

			System.out.println(updatedflightBaskets.size());
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return updatedflightBaskets;
	}

	private static Map<String, String> readBraches() {
		Map<String, String> branches = new HashMap<>();
		Map<String, String> agencies = new HashMap<>();
		PreparedStatement preStatement;
		try {

			preStatement = connTrevoo.prepareStatement("select Id, Name from Accounting2014.BranchAccount (NOLOCK)");
			ResultSet result = preStatement.executeQuery();
			while (result.next()) {
				agencies.put(result.getString("Id").trim(), result.getString("Name"));
			}

			preStatement = connTrevoo
					.prepareStatement("select Name, BranchId from Accounting2014.AgencyAccount (NOLOCK)");
			result = preStatement.executeQuery();
			while (result.next()) {
				branches.put(result.getString("Name"), agencies.get(result.getString("BranchId")));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return branches;
	}

	private static Map<String, String> readProviders() {
		Map<String, String> providers = new HashMap<>();
		PreparedStatement preStatement;
		try {
			preStatement = connTrevoo.prepareStatement(
					"select ProviderNo, ProviderName from Accounting2014.SupplierProviderMapping (NOLOCK)");
			ResultSet result = preStatement.executeQuery();
			while (result.next()) {
				providers.put(result.getString("ProviderNo").trim(), result.getString("ProviderName"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		providers.put("1852", "Amadeus Iran IR");
		return providers;
	}

	private static Map<Integer, String> getProductStatusses() {
		Map<Integer, String> productStatuses = new HashMap<Integer, String>();
		productStatuses.put(0, "NotLoaded");
		productStatuses.put(1, "Option");
		productStatuses.put(2, "SelectedAllocated");
		productStatuses.put(6, "RemovedFromFile");
		productStatuses.put(10, "PreBookingReservation");
		productStatuses.put(11, "Reservation");
		productStatuses.put(12, "Sales");
		productStatuses.put(16, "Cancelled");
		productStatuses.put(17, "Expired");
		productStatuses.put(18, "Refunding");
		productStatuses.put(19, "Refunded");
		productStatuses.put(20, "PartialRefunding");
		productStatuses.put(21, "PartialRefunded");

		return productStatuses;

	}

	private static Map<String, HashMap<String, String>> readBusinesses() {
		Map<String, HashMap<String, String>> businesses = new HashMap<String, HashMap<String, String>>();
		HashMap<String, String> bDetails = new HashMap<String, String>();
		PreparedStatement preStatement;
		try {
			preStatement = connTrevoo.prepareStatement(
					"select Id, Level, Name, SuperBusinessId, Location_District, Location_City, Location_Country, AgencyType, Status_IfActive, Status_IfForbidden, Activation_TransactionDate,GroupId from Accounts.Businesses (NOLOCK) where Level<4");
			ResultSet result = preStatement.executeQuery();
			while (result.next()) {
				bDetails = new HashMap<String, String>();
				bDetails.put("Name", result.getString("Name").trim());
				bDetails.put("Level", result.getString("Level"));
				bDetails.put("SuperBusinessId", result.getString("SuperBusinessId"));
				bDetails.put("Location_District", result.getString("Location_District").trim());
				bDetails.put("Location_City", result.getString("Location_City").trim());
				bDetails.put("Location_Country", result.getString("Location_Country").trim());
				bDetails.put("AgencyType", result.getString("AgencyType"));
				bDetails.put("Status_IfActive", result.getString("Status_IfActive"));
				bDetails.put("Status_IfForbidden", result.getString("Status_IfForbidden"));
				bDetails.put("Activation_TransactionDate", result.getString("Activation_TransactionDate"));
				if (result.getString("GroupId") != null)
					bDetails.put("GroupId", businessGroups.get(Integer.parseInt(result.getString("GroupId"))));
				businesses.put(result.getString("Id").trim(), bDetails);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return businesses;
	}

	private static void update(String[] hundredbaskets) {

		StringBuilder basketIds = basketIdBuilder(hundredbaskets);
		try {

			PreparedStatement preStatementBaskets = connTrevoo.prepareStatement(
					"select * from Sale.ShoppingFiles (NOLOCK) where Id in (" + basketIds.toString() + ")",
					ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
			resultBaskets = preStatementBaskets.executeQuery();
			resultBaskets.beforeFirst();

			PreparedStatement preStatementlocs = connTrevoo.prepareStatement(
					"select isnull(c.[Name], isnull(a.CityName, b.Destination)) as City, CoreShoppingFileId from [2k24].[dbo].[Booking] b (nolock) "
					+ "left join [BB_HotelInfo].[giata].[Hotel] h (nolock) inner join [BB_HotelInfo].[giata].[City] c (nolock) on c.Id = h.CityId "
					+ "on h.GiataId = b.MetaHotelId or (h.Id = b.HotelId and h.[Name] = b.HotelName) left join [BB_HotelInfo].[giata].[Address] a (nolock) on a.HotelId = b.HotelId "
					+ "where TRY_CAST(b.HotelId as bigint) is not null and CoreShoppingFileId in  (" + basketIds.toString() + ") union all "
					+ "select b.Destination collate SQL_Latin1_General_CP1_CI_AS as City, CoreShoppingFileId from [2k24].[dbo].[Booking] b (nolock) "
					+ "where TRY_CAST(b.HotelId as bigint) is null and CoreShoppingFileId in (" + basketIds.toString() + ")",
					ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
//			System.out.println(preStatementlocs);
			resultloc = preStatementlocs.executeQuery();
			resultloc.beforeFirst();
			
			PreparedStatement preStatementProducts = connTrevoo.prepareStatement(
					"select * from Sale.ShoppingFile_Products (NOLOCK) where ShoppingFileId in (" + basketIds.toString()
							+ ") and ProductType = 2 and Status in (12,18,19,20,21) and BookingCode!='*MAHSUP'",
					ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
			resultProducts = preStatementProducts.executeQuery();

			PreparedStatement preStatementTickets = connTrevoo
					.prepareStatement(
							"select * from Sale.ShoppingFile_ProductItems (NOLOCK) where ShoppingFileId in ("
									+ basketIds.toString() + ")",
							ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
			resultTickets = preStatementTickets.executeQuery();

			PreparedStatement preStatementCommissions = connTrevoo
					.prepareStatement(
							"select ShoppingFileId, ProductItemId, BusinessId, Selling_Amount, Ref_SfCode from Sale.ShoppingFile_Commissions (NOLOCK) where ShoppingFileId in ("
									+ basketIds.toString() + ")",
							ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
			resultCommissions = preStatementCommissions.executeQuery();

			PreparedStatement preStatementBookings = connTrevoo
					.prepareStatement(
							"SELECT CoreProductReference,BookingDate,CheckInDate,CheckOutDate FROM [2k24].[dbo].[Booking] (nolock) where CoreShoppingFileId in ("
									+ basketIds.toString() + ")",
							ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
			resultBookings = preStatementBookings.executeQuery();

			PreparedStatement preStatementPayments = connTrevoo.prepareStatement(
					"select ShoppingFileId, Selling_AmountOfInterest, Selling_AmountOfInterest_Credit, CC_InstallmentCount, CC_CardHolder, PaymentType, CC_CardNumber from Sale.ShoppingFile_Payments (NOLOCK) where ShoppingFileId in ("
							+ basketIds.toString() + ") and LastCommand in ('PSTH','CRDT') ",
					ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
			resultPayments = preStatementPayments.executeQuery();

			PreparedStatement preStatementPassengers = connTrevoo
					.prepareStatement(
							"select ProductId, FirstName, LastName, Email, BirthDate from Sale.ShoppingFile_Passengers P (NOLOCK) join Sale.ShoppingFile_PaxReferences PR (NOLOCK) On P.Id=PR.PassengerId where IfContact=1 and P.ShoppingFileId in ("
									+ basketIds.toString() + ")",
							ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
			resultPassengers = preStatementPassengers.executeQuery();
			createBaskets();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private static void createBaskets() {
		basket = new Document();
		try {
			while (resultBaskets.next()) {
				basket.clear();
				String id = resultBaskets.getString("Id");
				basket.put("_id", id);
				// System.out.println(id);
				if (resultBaskets.getObject("Customer_BusinessId") != null) {
					getBusiness(resultBaskets.getInt("Customer_BusinessId"));
				}

				if (resultBaskets.getObject("Customer_UserId") != null) {
					basket.put("CustomerUserId", resultBaskets.getInt("Customer_UserId"));
					basket.put("CustomerUserName", customerUsers.get(resultBaskets.getInt("Customer_UserId")));
				}

				Timestamp date = resultBaskets.getTimestamp("CreationDate");
				if (date != null) {
					Timestamp later = new Timestamp(date.getTime() + (1000 * 60 * 60 * TIME_DIFFERENCE));
					basket.put("CreationDate", format.parse(later.toString()));
				}

				date = resultBaskets.getTimestamp("T_LastTransactionDate");
				if (date != null) {
					Timestamp later = new Timestamp(date.getTime() + (1000 * 60 * 60 * TIME_DIFFERENCE));
					basket.put("LastTransactionDate", format.parse(later.toString()));
				}

				if (resultBaskets.getObject("SellingCurrency") != null)
					basket.put("SellingCurrency", resultBaskets.getObject("SellingCurrency"));

				if (resultBaskets.getObject("If_Finalized") != null)
					basket.put("IsFinalized", resultBaskets.getBoolean("If_Finalized"));

				getProducts(id, resultBaskets.getTimestamp("CreationDate"));
				getLocation(id);

				try {
					collection.insertOne(basket);
					insertCount++;

				} catch (MongoWriteException dk) {
					updateCount++;
					collection.replaceOne(new Document("_id", basket.getString("_id")), basket);
				}

			}
		} catch (JSONException | SQLException | ParseException e) {
			e.printStackTrace();
		}

	}

	private static void getLocation(String id) {
		try {
			while (resultloc.next()) {
				if (resultloc.getString("CoreShoppingFileId").toLowerCase().equals(id.toLowerCase())) {
					basket.put("HotelCity", resultloc.getObject("City"));
					// System.out.println(resultloc.getObject("City"));
				}
			}
			resultloc.beforeFirst();
		} catch (JSONException | SQLException e) {
			e.printStackTrace();
		}

	}

	private static void getBusiness(Integer businessId) throws SQLException, ParseException {
		Document business = new Document();
		Integer superBusinessId = null;

		HashMap<String, String> bDetails = businesses.get(businessId.toString());
		business.put("CustomerBusinessId", businessId);
		try {
			business.put("CustomerBusinessName", bDetails.get("Name"));

			if (branches.get(bDetails.get("Name")) == null)
				business.put("Branch", "BiletBank");
			else
				business.put("Branch", branches.get(bDetails.get("Name")));
			superBusinessId = Integer.parseInt(bDetails.get("SuperBusinessId"));
			business.put("CustomerSuperBusinessId", superBusinessId);
			business.put("District", bDetails.get("Location_District"));
			business.put("City", bDetails.get("Location_City"));
			business.put("Country", bDetails.get("Location_Country"));
			business.put("Type", bDetails.get("AgencyType"));
			business.put("Level", bDetails.get("Level"));
			business.put("GroupId", bDetails.get("GroupId"));
			business.put("ActivationDate", bDetails.get("Activation_TransactionDate"));

			if (bDetails.get("Status_IfForbidden").equals("1")) {
				business.put("Status", "Yasakl�");
			} else if (bDetails.get("Status_IfActive").equals("1")) {
				business.put("Status", "Aktif");
			} else {
				business.put("Status", "Pasif");
			}

			bDetails = businesses.get(superBusinessId.toString());
			business.put("CustomerSuperBusinessName", bDetails.get("Name"));
		} catch (NullPointerException e) {
			System.err.println("No Such Business Exists" + basket.get("_id"));
		}
		basket.put("Business", business);
	}

	private static void getProducts(String id, Timestamp creationDate) {
		Timestamp sellingDate = null;
		List<Document> products = new ArrayList<>();

		int productCount = 0;
		try {
			while (resultProducts.next()) {
				if (resultProducts.getString("ShoppingFileId").equals(id)) {
					productCount++;
				}
			}
			resultProducts.beforeFirst();
			while (resultProducts.next()) {
				if (resultProducts.getString("ShoppingFileId").equals(id)
						&& resultProducts.getInt("ProductType") == 2) {
					product = new Document();
					product.put("productId", resultProducts.getString("Id"));
					getDates(resultProducts.getString("Id"));

					if (resultProducts.getObject("ProductType") != null)
						product.put("ProductType", resultProducts.getInt("ProductType"));
					if (resultProducts.getObject("ProviderId") != null) {
						product.put("ProviderId", resultProducts.getInt("ProviderId"));
						product.put("ProviderName", providers.get(resultProducts.getObject("ProviderId").toString()));
					}
					String VATFlag = resultProducts.getString("VATFlag");
					if (resultProducts.getObject("VATFlag") != null)
						product.put("VATFlag", resultProducts.getString("VATFlag"));
					if (resultProducts.getObject("BookingCode") != null)
						product.put("Voucher", resultProducts.getString("BookingCode").trim());

					if (resultProducts.getObject("BookingCode") != null) {
						if (resultProducts.getInt("ProviderId") == 7) {
							try {
								product.put("ProviderRefNo",
										resultProducts.getString("BookingCode").trim().split(";")[1]);
							} catch (ArrayIndexOutOfBoundsException e) {
								System.out.println("invalid voucher:" + resultProducts.getString("BookingCode"));
							}
						} else if (resultProducts.getInt("ProviderId") == 1) {
							product.put("ProviderRefNo", "XI-" + resultProducts.getString("BookingCode").trim());
						} else if (resultProducts.getInt("ProviderId") == 2) {
							product.put("ProviderRefNo", resultProducts.getString("BookingCode").trim());
						}
					}

					if (resultProducts.getObject("XRate_B2S_OnSelling") != null)
						product.put("XRate", resultProducts.getDouble("XRate_B2S_OnSelling"));

					resultProducts.getString("BookingCode");
					Timestamp d = resultProducts.getTimestamp("CreationDate");
					Timestamp later;
					if (d != null) {
						later = new Timestamp(d.getTime() + (1000 * 60 * 60 * TIME_DIFFERENCE));
						product.put("CreationDate", format.parse(later.toString()));
					}

					d = resultProducts.getTimestamp("PrebookingDate");
					if (d != null) {
						later = new Timestamp(d.getTime() + (1000 * 60 * 60 * TIME_DIFFERENCE));
						product.put("PrebookingDate", format.parse(later.toString()));
					}

					d = resultProducts.getTimestamp("ReservationDate");
					if (d != null) {
						later = new Timestamp(d.getTime() + (1000 * 60 * 60 * TIME_DIFFERENCE));
						product.put("ReservationDate", format.parse(later.toString()));
					}

					d = resultProducts.getTimestamp("SellingDate");
					if (d != null) {
						later = new Timestamp(d.getTime() + (1000 * 60 * 60 * TIME_DIFFERENCE));
						sellingDate = new Timestamp(d.getTime() + (1000 * 60 * 60 * TIME_DIFFERENCE));
					}

					d = resultProducts.getTimestamp("CancelDate");
					if (d != null) {
						later = new Timestamp(d.getTime() + (1000 * 60 * 60 * TIME_DIFFERENCE));
						product.put("CancelDate", format.parse(later.toString()));
					}

					if (resultProducts.getObject("Status") != null)
						product.put("Status", productStatuses.get(resultProducts.getInt("Status")));

					if (sellingDate == null) {
						System.out.println(creationDate);
						sellingDate = new Timestamp(creationDate.getTime() + (1000 * 60 * 60 * TIME_DIFFERENCE));
					}

					// buying amounts

					// product.put("BuyingServiceFee", round(SC / rate, 2));
					// product.put("BuyingDFF", round(DFF / rate, 2));
					// product.put("BuyingBFF", round(BFF / rate, 2));

					getPassenger(resultProducts.getString("Id"));
					getTickets(id, resultProducts.getString("Id"), sellingDate);
					products.add(product);

				}
			}

			basket.put("Products", products);
			resultProducts.beforeFirst();
		} catch (JSONException | SQLException | ParseException e) {
			e.printStackTrace();
		}

	}

	private static void getTickets(String id, String productid, Timestamp sellingDate) {
		Document date;
		List<Document> tickets = new ArrayList<>();
		List<Document> datess = new ArrayList<>();

		int ticketCount = 0;

		try {
			while (resultTickets.next()) {
				if (resultTickets.getString("ShoppingFileId").equals(id)) {
					ticketCount++;
				}
			}
			resultTickets.beforeFirst();
			while (resultTickets.next()) {
				datess = new ArrayList<>();
				if (resultTickets.getString("ShoppingFileId").equals(id)
						&& resultTickets.getString("ProductId").equals(productid)) {

					ticket = new Document();
					ticket.put("TicketId", resultTickets.getString("Id"));

					if (resultTickets.getObject("PassengerCount") != null)
						ticket.put("PassengerCount", resultTickets.getInt("PassengerCount"));
					if (resultTickets.getObject("Selling_Currency") != null)
						ticket.put("SellingCurrency", resultTickets.getString("Selling_Currency"));
					if (resultTickets.getObject("Buying_Currency") != null)
						ticket.put("BuyingCurrency", resultTickets.getString("Buying_Currency"));
					if (resultTickets.getObject("Buying_BaseFare") != null)
						ticket.put("BuyingBaseFare", resultTickets.getString("Buying_BaseFare"));
					double baseFare = 0;
					if (resultTickets.getObject("Selling_BaseFare") != null) {
						baseFare = resultTickets.getDouble("Selling_BaseFare");
						ticket.put("BaseFare", baseFare);
					}
					double DFF = 0;
					if (resultTickets.getObject("Selling_BaseFare_AddOn") != null) {
						DFF = resultTickets.getDouble("Selling_BaseFare_AddOn");
						ticket.put("DFF", DFF);
					}
					// System.out.println("calling getBFF");
					getBFF(id, ticketCount);
					double taxes = 0, YR = 0, VQ = 0, TXE = 0;

					double SC = 0;
					if (resultTickets.getObject("Selling_ServiceFee") != null) {
						SC = resultTickets.getDouble("Selling_ServiceFee");
						ticket.put("SC", SC);
					}

					if (resultTickets.getObject("Ext_Reissue_OldTicketNo") != null) {
						ticket.put("reissueOldTicketNumber", resultTickets.getString("Ext_Reissue_OldTicketNo").trim());
					}

					double BaseFareNet, SCNet = SC;
					double taxesNET = 0, YRNET = 0, VQNET = 0, TXENET = 0, DFFNet, AgencyDFF;

					date = new Document();
					try {
						date.put("SellingDate", format.parse(sellingDate.toString()));
					} catch (NullPointerException e) {
					}
					datess.add(date);
					date = new Document();

					if (resultTickets.getObject("Refund_IsPerformed") != null
							&& resultTickets.getObject("Selling_BaseFare_Refund") != null) {
						ticket.put("BaseFareRefund", resultTickets.getDouble("Selling_BaseFare_Refund"));
						BaseFareNet = resultTickets.getDouble("Selling_BaseFare")
								- resultTickets.getDouble("Selling_BaseFare_Refund");
						ticket.put("BaseFareNet", round(BaseFareNet, 2));

						ticket.put("SCRefund", resultTickets.getDouble("Selling_ServiceFee_Refund"));
						SCNet = resultTickets.getDouble("Selling_ServiceFee")
								- resultTickets.getDouble("Selling_ServiceFee_Refund");
						ticket.put("SCNet", round(SCNet, 2));

						ticket.put("DFFRefund", resultTickets.getDouble("Selling_BaseFare_AddOn_Refund"));
						DFFNet = resultTickets.getDouble("Selling_BaseFare_AddOn")
								- resultTickets.getDouble("Selling_BaseFare_AddOn_Refund");
						DFFNet = DFFNet + resultTickets.getDouble("Selling_Refund_Charge")
								+ resultTickets.getDouble("Refund_AgencyDffCommRate")
										* resultTickets.getDouble("Refund_AgencyDffAmount");

						ticket.put("DFFNet", round(DFFNet, 2));

						ticket.put("Refund_IsPerformed", resultTickets.getInt("Refund_IsPerformed"));

						double ciro = BaseFareNet + SCNet + DFFNet;
						ticket.put("CiroNet", round(ciro, 2));

						Timestamp d = resultTickets.getTimestamp("Refund_PerformedDate");
						if (d != null && resultTickets.getObject("Selling_BaseFare_Refund") != null) {
							Timestamp later = new Timestamp(d.getTime() + (1000 * 60 * 60 * TIME_DIFFERENCE));
							date.put("CancelDate", format.parse(later.toString()));
						}
						if (!date.isEmpty())
							datess.add(date);
					}

					ticket.put("Ciro", round((baseFare + SC + DFF), 2));

					if (resultTickets.getObject("Selling_Refund_Charge") != null)
						ticket.put("RefundCharge", round(resultTickets.getDouble("Selling_Refund_Charge"), 2));

					if (resultTickets.getObject("Flight_Baggage") != null)
						ticket.put("Baggage", resultTickets.getString("Flight_Baggage"));

					getCommissions(id, resultTickets.getString("Id"));
					getPassenger(resultTickets.getString("Id"));
					ticket.put("Dates", datess);

					tickets.add(ticket);

				}

			}

			product.put("Tickets", tickets);
			resultTickets.beforeFirst();
		} catch (JSONException | SQLException | ParseException e) {
			e.printStackTrace();
		}

	}

	private static Date getRefundDate(String productid) {
		Date date = null;

		try {
			while (resultTickets.next()) {
				if (resultTickets.getString("ProductId").equals(productid)) {
					Timestamp d = resultTickets.getTimestamp("Refund_PerformedDate");
					if (d != null) {
						Timestamp later = new Timestamp(d.getTime() + (1000 * 60 * 60 * TIME_DIFFERENCE));
						date = format.parse(later.toString());
					}
				}

			}
		} catch (JSONException | SQLException | ParseException e) {
			e.printStackTrace();
		}

		return date;
	}

	private static void getPassenger(String productId) {
		try {

			while (resultPassengers.next()) {
				if (resultPassengers.getString("ProductId").equals(productId)) {
					product.put("FirstName", resultPassengers.getString("FirstName"));
					product.put("LastName", resultPassengers.getString("LastName"));
				}
			}
			resultPassengers.beforeFirst();
		} catch (JSONException | SQLException e) {
			e.printStackTrace();
		}

	}

	private static void getDates(String productid) {

		try {
			while (resultBookings.next()) {

				if (resultBookings.getString("CoreProductReference").equals(productid.toLowerCase())) {

					Timestamp date = resultBookings.getTimestamp("BookingDate");
					if (date != null) {
						Timestamp later = new Timestamp(date.getTime() + (1000 * 60 * 60 * TIME_DIFFERENCE));
						product.put("BookingDate", format.parse(later.toString()));
					}

					date = resultBookings.getTimestamp("CheckInDate");
					if (date != null) {
						Timestamp later = new Timestamp(date.getTime() + (1000 * 60 * 60 * TIME_DIFFERENCE));
						product.put("CheckInDate", format.parse(later.toString()));
					}

					date = resultBookings.getTimestamp("CheckOutDate");
					if (date != null) {
						Timestamp later = new Timestamp(date.getTime() + (1000 * 60 * 60 * TIME_DIFFERENCE));
						product.put("CheckOutDate", format.parse(later.toString()));
					}
				}
			}
			resultBookings.beforeFirst();
		} catch (JSONException | SQLException | ParseException e) {
			e.printStackTrace();
		} catch (NullPointerException e) {
			System.out.println("productid does not occur in bookings table:" + productid);
			;
		}

	}

	private static Object round(double value, int places) {

		if (places < 0)
			throw new IllegalArgumentException();

		long factor = (long) Math.pow(10, places);
		value = value * factor;
		long tmp = Math.round(value);
		return (double) tmp / factor;

	}

	private static void getBFF(String id, int productCount) {
		double bff = 0;
		double bffRefund = 0;
		Integer installmentCount = null;

		try {
			while (resultPayments.next()) {
				installmentCount = null;
				if (resultPayments.getString("ShoppingFileId").equals(id)) {
					bff += resultPayments.getDouble("Selling_AmountOfInterest");
					// System.out.println(bff);
					bffRefund += resultPayments.getDouble("Selling_AmountOfInterest_Credit");
					if (resultPayments.getObject("CC_InstallmentCount") != null) {
						installmentCount = resultPayments.getInt("CC_InstallmentCount");
					}

					// System.out.println(installmentCount);

				}
			}
		} catch (JSONException | SQLException e) {
			e.printStackTrace();
		}
		try {
			resultPayments.beforeFirst();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		double bffNet = bff - bffRefund;
		ticket.put("BFF", round(bff / productCount, 2));
		ticket.put("BFFrefund", round(bffRefund / productCount, 2));
		ticket.put("BFFNet", round(bffNet / productCount, 2));
		// ticket.put("installmentCount", installmentCount);

	}

	private static void getCommissions(String id, String productId) {
		List<Document> commissions = new ArrayList<>();
		Map<Integer, Double> comm = new HashMap<Integer, Double>();
		Integer business;
		double amount;
		try {
			while (resultCommissions.next()) {
				if (resultCommissions.getString("ShoppingFileId").equals(id)
						&& resultCommissions.getString("ProductItemId").equals(productId)) {
					if (resultCommissions.getString("Ref_SfCode") != null
							&& resultCommissions.getString("Ref_SfCode").equals("DFF")) {
						continue;
					} else
						business = resultCommissions.getInt("BusinessId");
					amount = resultCommissions.getDouble("Selling_Amount");
					if (!comm.containsKey(business))
						comm.put(business, amount);
					else
						comm.put(business, amount + comm.get(business));
				}
			}
			resultCommissions.beforeFirst();
		} catch (JSONException | SQLException e) {
			e.printStackTrace();
		}

		Document commission;
		for (Entry<Integer, Double> entry : comm.entrySet()) {
			commission = new Document();

			commission.put("Business", (businesses.get(entry.getKey().toString()) == null) ? ""
					: businesses.get(entry.getKey().toString()).get("Name"));
			commission.put("Amount", entry.getValue());
			commission.put("Level", (businesses.get(entry.getKey().toString()) == null) ? ""
					: Integer.parseInt(businesses.get(entry.getKey().toString()).get("Level")));
			commissions.add(commission);
		}
		ticket.put("Commissions", commissions);
	}

	private static StringBuilder basketIdBuilder(String[] hundredbaskets) {
		StringBuilder builder = new StringBuilder();
		builder.append("'");
		for (int i = 0; i < hundredbaskets.length; i++) {
			if (hundredbaskets[i] != null) {
				builder.append(hundredbaskets[i]);
				builder.append("','");
			}
		}
		builder.deleteCharAt(builder.length() - 1);
		builder.deleteCharAt(builder.length() - 1);

		return builder;
	}

	private static StringBuilder basketIdBuilder(Object[] hundredbaskets) {
		StringBuilder builder = new StringBuilder();
		builder.append("'");
		for (int i = 0; i < hundredbaskets.length; i++) {
			if (hundredbaskets[i] != null) {
				builder.append(hundredbaskets[i].toString());
				builder.append("','");
			}
		}
		builder.deleteCharAt(builder.length() - 1);
		builder.deleteCharAt(builder.length() - 1);

		return builder;
	}

	private static Set<String> readNonExistingBasketIds(String lastUpdate, String updateUntil) {
		Set<String> allBaskets = new HashSet<>();
		Set<String> existingBaskets = new HashSet<>();
		allBaskets = readAllBasketIds(lastUpdate, updateUntil);
		System.out.println("all baskets: " + allBaskets.size());
		// existingBaskets = readAllBasketIdsInMongo();
		// System.out.println("existing: " + existingBaskets.size());

		// allBaskets.removeAll(existingBaskets);

		return allBaskets;
	}

	private static Set<String> readAllBasketIdsInMongo() {
		Set<String> existingBaskets = new HashSet<>();
		MongoCollection<Document> collection = db.getCollection("baskets5");
		MongoCursor<Document> cursor = collection.find().iterator();

		try {
			while (cursor.hasNext()) {
				existingBaskets.add(cursor.next().get("_id").toString());
			}
		} finally {
			cursor.close();
		}

		return existingBaskets;
	}

	private static Set<String> readAllBasketIds(String lastUpdate, String updateUntil) {
		Set<String> updatedBaskets = new HashSet<>();
		PreparedStatement preStatement;
		try {
			preStatement = connTrevoo.prepareStatement(
					"select ShoppingFileId from Sale.ShoppingFile_Products (NOLOCK) where ProductType=2 and Status in (12,18,19,20,21) and BookingCode!='*MAHSUP' and dateadd(HOUR, 3, CreationDate) >= '"
							+ lastUpdate + "' and dateadd(HOUR, 3, CreationDate) < '" + updateUntil + "'");

//			System.out.println(preStatement);
			ResultSet result = preStatement.executeQuery();
			while (result.next()) {
				updatedBaskets.add(result.getString("ShoppingFileId"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return updatedBaskets;
	}

	private static Connection connect(String dbName) {
		Connection conn = null;
		try {
			Class.forName("net.sourceforge.jtds.jdbc.Driver");
			conn = DriverManager.getConnection(
					"jdbc:jtds:sqlserver://" + prop.getProperty("maindatabaseIP") + "/" + dbName,
					prop.getProperty("maindbuser"), prop.getProperty("maindbpassword"));
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return conn;
	}

}
