package com.turna.db;

import java.sql.ResultSet;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class JsonMongoWriterTest {

	ReadFromSQLTable rfsql;
	MongoConnection monCon;

	@Before
	public void initConn() {
		rfsql = new ReadFromSQLTable();
		monCon = new MongoConnection();
	}

	@Test
	public void writeToMongo() {
		ResultSet result = rfsql.getResultset("select * from Shopping.Baskets where Id=27750");
		JSONObject jsonResult = rfsql.convertResultSetIntoJSON(result);
		JsonMongoWriter writer = new JsonMongoWriter();
//		System.out.println(jsonResult);
		writer.write("baskets",jsonResult);

	}

	@After
	public void finalizeConn() {
		rfsql.close();

	}

}
