package com.turna.db;

import java.sql.Connection;
import java.util.Properties;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import util.PropertyReader;

public class SQLConnectionTest {

	Connection conn;
	Properties prop;

	@Before
	public void initConn() {
		prop = new Properties();
		prop = PropertyReader.getProperties(null);
		SQLConnection sqlCon = new SQLConnection();
		conn = sqlCon.connect(prop.getProperty("maindatabaseIP"), prop.getProperty("maindb"),
				prop.getProperty("maindbuser"), prop.getProperty("maindbpassword"));
	}

	@Test
	public void TestNoExceptionIsThrownByForDbConnectionTest() {
		SQLConnection sqlCon = new SQLConnection();
		try {
			sqlCon.connect(prop.getProperty("maindatabaseIP"), prop.getProperty("maindb"),
					prop.getProperty("maindbuser"), prop.getProperty("maindbpassword"));
		} catch (Exception ex) {
			Assert.fail("Expected no exception, but got: " + ex.getMessage());
		}
	}
}
