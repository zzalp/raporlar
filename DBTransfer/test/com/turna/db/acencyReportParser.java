package com.turna.db;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

import org.bson.Document;
import org.joda.time.DateTimeZone;

import com.mongodb.BasicDBObject;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;

public class acencyReportParser {
	static Map<Integer, String> agencies;
	static MongoDatabase db;
	static Set<Integer> ids;

	public static void main(String[] args) {

		MongoClient mongoClient;
		mongoClient = new MongoClient("report.biletbank.com", 27017);
		db = mongoClient.getDatabase("BiletBank");

		Set<Document> agencyTickets = new LinkedHashSet<Document>();
		MongoCollection<Document> collection = db.getCollection("acenteviewreport");

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
		DateTimeZone zone = DateTimeZone.UTC;
		DateTimeZone.setDefault(zone);

		String date = "2017-03-13";
		String enddate = "2017-03-14";

		Date startDate = null;
		Date endDate = null;
		try {
			startDate = simpleDateFormat.parse(date);
			endDate = simpleDateFormat.parse(enddate);

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Integer id = 6186;
		ids = new HashSet<Integer>();
		ids.add(id);

		addChildIds(id);
		System.out.println(ids);

		BasicDBObject inQuery = new BasicDBObject();
		inQuery.put("AgencyId", new BasicDBObject("$in", ids));
		inQuery.put("date", new BasicDBObject("$gte", startDate).append("$lte", endDate));

		String status = "Sales";
		if (!"all".equals(status)) {
			inQuery.put("TicketStatus", status);
		}

		String type = "all";
		if (!"all".equals(type)) {
			inQuery.put("flightType", type);
		}

		long totalTicketCount = collection.count(inQuery);
		Document doc = new Document();
		doc.put("totalTicketCount", totalTicketCount);
		agencyTickets.add(doc);
		System.out.println("total tickets:" + totalTicketCount);
		System.out.println(agencyTickets);
		MongoCursor<Document> cursor = collection.find(inQuery).iterator();

		try {
			while (cursor.hasNext()) {
				agencyTickets.add(cursor.next());
			}
		} finally {
			cursor.close();
		}

		mongoClient.close();
		System.out.println(agencyTickets);
	}

	private static void addChildIds(Integer agencyid) {
		MongoCollection<Document> collection = db.getCollection("acenteler");

		BasicDBObject inQuery = new BasicDBObject();
		inQuery.put("SuperAgencyId", agencyid);

		System.out.println(inQuery);

		MongoCursor<Document> cursor = collection.find(inQuery).iterator();

		try {
			while (cursor.hasNext()) {
				ids.add(cursor.next().getInteger("AgencyId"));
			}
		} finally {
			cursor.close();
		}

	}
}
