package com.turna.db;

import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

import java.sql.ResultSet;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class readFromSQLTableTest {

	ReadFromSQLTable rfsql;

	@Before
	public void initConn() {
		rfsql = new ReadFromSQLTable();
	}

//	@Test
	public void ReadFromTable() {
		assertThat(rfsql.getResultset("select * from Shopping.Baskets where Id=27750 and En"), notNullValue());
	}

	@Test
	public void ConvertToJSon() {
		ResultSet result = rfsql.getResultset("select * from Shopping.Baskets where Id=27750");
		assertThat(rfsql.convertResultSetIntoJSON(result), notNullValue());
	}

	@After
	public void finalizeConn() {
		rfsql.close();
	}

}
