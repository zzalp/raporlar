package com.turna.db;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.mongodb.DB;
import com.mongodb.client.MongoDatabase;

public class MongoConnectionTest {

	@Before
	public void initConn() {
		
		MongoConnection monCon = new MongoConnection();
		MongoDatabase db = monCon.connect();
	}

	@Test
	public void TestNoExceptionIsThrownByForDbConnectionTest() {
		MongoConnection monCon = new MongoConnection();
		try {
			monCon.connect();
		} catch (Exception ex) {
			Assert.fail("Expected no exception, but got: " + ex.getMessage());
		}
	}

}
